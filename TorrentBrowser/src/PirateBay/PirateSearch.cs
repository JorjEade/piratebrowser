﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Text;


namespace TorrentBrowser
{
	class PirateSearch : IDisposable
	{
		public enum OrderBy
		{
			UploadeDate = 3,
			Size = 5,
			Seeders = 7,
			Leechers = 9,
			UploadedBy = 11
		}

		public const string PATH_BROWSE = "browse/" + URL_PARAM_BROWSE_CATEGORY +	"/" + URL_PARAM_PAGE_NUMBER + "/" + URL_PARAM_ORDER_BY;
		public const string PATH_SEARCH = "search/" + URL_PARAM_SEARCH_QUERY +		"/" + URL_PARAM_PAGE_NUMBER + "/" + URL_PARAM_ORDER_BY + "/" + URL_PARAM_SEARCH_CATEGORIES;
		private const string URL_PARAM_PAGE_NUMBER = "{page_number}";
		private const string URL_PARAM_BROWSE_CATEGORY = "{browse_category}";
		private const string URL_PARAM_SEARCH_CATEGORIES = "{search_categories}";
		private const string URL_PARAM_SEARCH_QUERY = "{search_query}";
		private const string URL_PARAM_ORDER_BY = "{order_by}";
		private const int RESULTS_PER_PAGE = 30;


		public readonly List<Torrent> Torrents = new List<Torrent>();

		public event Action<Torrent> TorrentCreated;
		
		private readonly string domain;
		private readonly string path;
		private readonly string urlTemplate;
		private CancellationTokenSource cancellationToken;



		public PirateSearch(string domain, string path)
		{
			this.domain = domain;
			this.path = path;
			urlTemplate = domain.TrimEnd('/') + "/" + path.TrimStart('/');
		}



		public static string GetPath_BrowseCategory(int category, OrderBy orderBy = OrderBy.Seeders)
		{
			return
				PATH_BROWSE
				.Replace(URL_PARAM_BROWSE_CATEGORY, category.ToString())
				.Replace(URL_PARAM_ORDER_BY, ((int)orderBy).ToString());
		}



		public static string GetPath_Search(string query, int category, OrderBy orderBy = OrderBy.Seeders)
		{
			return
				PATH_SEARCH
				.Replace(URL_PARAM_SEARCH_QUERY, query)
				.Replace(URL_PARAM_SEARCH_CATEGORIES, category.ToString())
				.Replace(URL_PARAM_ORDER_BY, ((int)orderBy).ToString());
		}



		private string GetPageUrl(int pageIndex)
		{
			int pageNumber = (pageIndex + 1);
			if (pageNumber == 1) {
				pageNumber = 0;
			}
			return urlTemplate.Replace(URL_PARAM_PAGE_NUMBER, pageNumber.ToString());
		}



		public void Load(Action<PirateSearch> onComplete = null)
		{
			cancellationToken = new CancellationTokenSource();
			Task.Run(() => Thread_Load(onComplete), cancellationToken.Token);
		}
		
		
		
		private void Thread_Load(Action<PirateSearch> onComplete = null)
		{
			int currPageIndex = -1;
			
			while (true)
			{
				// Load torrents from the next page
				Torrent[] results = LoadTorrentsFromPage(++currPageIndex);

				// Check if search was cancelled
				if (cancellationToken.IsCancellationRequested) {
					Console.WriteLine("...PirateSearch canceled");
					break;
				}

				// Add the torrents
				foreach (var torrent in results) {
					Torrents.Add(torrent);
					TorrentCreated.CrossThreadInvoke(torrent);
				}
				
				// Check end condition
				if (results.Length < RESULTS_PER_PAGE) {
					Console.WriteLine("...no more results - less than a full page of results was returned (" + results.Length + "/" + RESULTS_PER_PAGE + ")");
					break;
				}
			}

			onComplete?.CrossThreadInvoke(this);
			
			cancellationToken = null;
		}



		private Torrent[] LoadTorrentsFromPage(int index)
		{
			string pageUrl = GetPageUrl(index);

			// Load the page
			Console.WriteLine("Loading page " + (index + 1) + ": " + pageUrl);
			string html;
			try
			{
				html = Utils.LoadData(pageUrl, Encoding.UTF8);
			}
			catch (Exception e)
			{
				Console.WriteLine("Error loading page \"" + pageUrl + "\":\n   " + e);
				return new Torrent[0];
			}

			//Console.WriteLine("...page loaded " + (index + 1) + ": " + pageUrl);

			// Scrape torrents from HTML
			Torrent[] results = ScrapeTorrentsFromHTML(html);

			return results;
		}



		private Torrent[] ScrapeTorrentsFromHTML(string html)
		{
			if (string.IsNullOrEmpty(html))
				return new Torrent[0];

			const string GAP_INCLUDING_LINEBREAKS = @"[\S\s]+?";

			const string REGEX =
				// Start
				"<tr(?: class=\"alt\")?>" + GAP_INCLUDING_LINEBREAKS +
				// [1] Category
				"category\">(.+?)<\\/a><br>" + GAP_INCLUDING_LINEBREAKS +
				// [2] Sub category
				"category\">(.+?)<\\/a>\\)" + GAP_INCLUDING_LINEBREAKS +
				// [3] Page URL
				"<div class=\"detName\">.+?\"(.+?)\".+?" + 
				// [4] Title
				">(.+)<" + GAP_INCLUDING_LINEBREAKS +
				// [5] Magnet link
				"\"(magnet:.+?)\"" +
				// [6] Comments
				"(?:.+?this torrent has (\\d+) comments.+?|.+?)" +
				// [7] Trusted
				"(?:.+?(trusted).+?|.+?)" +
				// [8] VIP
				"(?:.+?(vip).+?|.+?)" +
				// [9] Upload date
				@"Uploaded (?:<b>)?(.+?)(?:<\/b>)?, " +
				// [10] Size
				"Size (.+?), .+?" +
				// [11] Uploaded by
				">(.+?)<" + GAP_INCLUDING_LINEBREAKS +
				// [12] Seeders
				"<td align=\"right\">(\\d+)<\\/td>" + GAP_INCLUDING_LINEBREAKS +
				// [13] Leechers
				"<td align=\"right\">(\\d+)<\\/td>" + GAP_INCLUDING_LINEBREAKS +
				// End
				"<\\/tr>";


			MatchCollection matches = Regex.Matches(html, REGEX, RegexOptions.IgnoreCase);

			Torrent[] results = new Torrent[matches.Count];

			for (int i = 0; i < matches.Count; ++i)
			{
				Match match = matches[i];
				
				results[i] = CreateTorrent(
					category:		PirateCategory.ParseCategory(match.Groups[1].Value),
					subcategory:	match.Groups[2].Value,
					pageUrl:		match.Groups[3].Value,
					rawTitle:		WebUtility.HtmlDecode(match.Groups[4].Value),
					magnetLink:		match.Groups[5].Value,
					numComments:	match.Groups[6].Success ? int.Parse(match.Groups[6].Value) : 0,
					isTrusted:		!string.IsNullOrEmpty(match.Groups[7].Value),
					isVip:			!string.IsNullOrEmpty(match.Groups[8].Value),
					uploadDate:		ParseDate(match.Groups[9].Value),
					size:			ParseFileSize(match.Groups[10].Value),
					uploadedBy:		match.Groups[11].Value,
					numSeeders:		int.Parse(match.Groups[12].Value),
					numLeechers:	int.Parse(match.Groups[13].Value)
				);
			}

			return results;
		}



		private DateTime ParseDate(string str)
		{
			str = WebUtility.HtmlDecode(str).ToLower();

			int year = 0, month = 0, day = 0, hour = 0, minute = 0;

			Match match;

			// Today
			if (str.StartsWith("today"))
			{
				day = DateTime.Now.Day;
				month = DateTime.Now.Month;
				year = DateTime.Now.Year;
			}
			// Yesterday
			else if (str.StartsWith("y-day"))
			{
				DateTime yday = DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0));
				day = yday.Day;
				month = yday.Month;
				year = yday.Year;
			}
			// Date
			else if ((match = Regex.Match(str, @"(\d\d)-(\d\d)")).Success)
			{
				month = int.Parse(match.Groups[1].Value);
				day = int.Parse(match.Groups[2].Value);
				Match matchYear = Regex.Match(str, @"\d\d\d\d");
				year = matchYear.Success ? int.Parse(matchYear.Value) : DateTime.Now.Year;
			}
			// X mins ago
			else if ((match = Regex.Match(str, @"(\d{0,2}) mins ago")).Success)
			{
				return DateTime.Now.Subtract(TimeSpan.FromMinutes(int.Parse(match.Groups[1].Value)));
			}

			// Time
			Match matchTime = Regex.Match(str, @"\s(\d\d):(\d\d)$");
			if (matchTime.Success) {
				hour = int.Parse(matchTime.Groups[1].Value);
				minute = int.Parse(matchTime.Groups[2].Value);
			}

			return new DateTime(year, month, day, hour, minute, 0);
		}



		private ulong ParseFileSize(string str)
		{
			float number = float.Parse(Regex.Match(str, @"[\d\.]+").Value);
			
			if (str.EndsWith("GiB"))
				return (ulong)(number * 1024 * 1024 * 1024);

			if (str.EndsWith("MiB"))
				return (ulong)(number * 1024 * 1024);

			if (str.EndsWith("KiB"))
				return (ulong)(number * 1024);

			return (ulong)number;
		}



		public void Dispose()
		{	
			cancellationToken?.Cancel();
		}



		public static Torrent CreateTorrent(
			PirateCategory.Category category,
			string subcategory,
			string pageUrl,
			string rawTitle,
			string magnetLink,
			int numComments,
			bool isTrusted,
			bool isVip,
			DateTime uploadDate,
			ulong size,
			string uploadedBy,
			int numSeeders,
			int numLeechers
		) {
			switch (category)
			{
				case PirateCategory.Category.Video:
				case PirateCategory.Category.Porn:

					PirateCategory.SubcategoryVideo subcategoryEnum = PirateCategory.ParseSubcategoryVideo(subcategory);

					return new Torrent_Video(
						videoType:		PirateCategory.MOVIE_CATEGORIES.Contains((int)subcategoryEnum) ? VideoType.Movie : VideoType.TV,
						category:		(int)category,
						subcategory:	(int)subcategoryEnum,
						pageUrl:		pageUrl,
						rawTitle:		rawTitle,
						magnetLink:		magnetLink,
						numComments:	numComments,
						isTrusted:		isTrusted,
						isVip:			isVip,
						uploadDate:		uploadDate,
						size:			size,
						uploadedBy:		uploadedBy,
						numSeeders:		numSeeders,
						numLeechers:	numLeechers
					);

				default:
					return new Torrent(
						category:		(int)category,
						subcategory:	PirateCategory.ParseSubcategory(category, subcategory),
						pageUrl:		pageUrl,
						rawTitle:		rawTitle,
						magnetLink:		magnetLink,
						numComments:	numComments,
						isTrusted:		isTrusted,
						isVip:			isVip,
						uploadDate:		uploadDate,
						size:			size,
						uploadedBy:		uploadedBy,
						numSeeders:		numSeeders,
						numLeechers:	numLeechers
					);
			}
		}

	}
	
}
