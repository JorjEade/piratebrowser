﻿using System;
using System.Windows.Forms;
using System.Drawing;


namespace TorrentBrowser
{
	class Range<T> : Panel where T:IComparable, IFormattable, IComparable<T>, IEquatable<T>
	{

		private readonly ComboBox cbxMin;
		private readonly ComboBox cbxMax;
		private readonly Label toLabel;


		public Range()
		{
			// Min
			cbxMin = new ComboBox();
			cbxMin.DropDownStyle = ComboBoxStyle.DropDownList;
			cbxMin.Size = new Size(62, 0);
			Controls.Add(cbxMin);
			
			// Max
			cbxMax = new ComboBox();
			cbxMax.DropDownStyle = ComboBoxStyle.DropDownList;
			cbxMax.Size = new Size(cbxMin.Size.Width, 0);
			Controls.Add(cbxMax);
			
			// To
			toLabel = new Label();
			toLabel.Text = "to";
			toLabel.TextAlign = ContentAlignment.TopCenter;
			toLabel.Location = new Point(1, 3);
			Controls.Add(toLabel);
		}



		protected override void OnClientSizeChanged(EventArgs e)
		{
			cbxMax.Location = new Point(ClientSize.Width - cbxMax.Width, 0);
			toLabel.Size = ClientSize;

			if (ClientSize.Height < cbxMax.Height) {
				ClientSize = new Size(ClientSize.Width, cbxMax.Height);
			}
		}



		public void AddToMin(ComboBoxItem<T> item)
		{
			cbxMin.Items.Add(item);
			cbxMin.SelectedIndex = 0;
		}



		public void AddToMax(ComboBoxItem<T> item)
		{
			cbxMax.Items.Add(item);
			cbxMax.SelectedIndex = cbxMax.Items.Count - 1;
		}



		public void Add(ComboBoxItem<T> item)
		{
			AddToMin(item);
			AddToMax(item);
		}



		public T ValueMin
		{
			get { return ((ComboBoxItem<T>)cbxMin.SelectedItem).Value; }
			set { cbxMin.SetSelectedValue(value); }
		}



		public T ValueMax
		{
			get { return ((ComboBoxItem<T>)cbxMax.SelectedItem).Value; }
			set { cbxMax.SetSelectedValue(value); }
		}



		public int IndexMin
		{
			get { return cbxMin.SelectedIndex; }
			set { cbxMin.SelectedIndex = value; }
		}



		public int IndexMax
		{
			get { return cbxMax.SelectedIndex; }
			set { cbxMax.SelectedIndex = value; }
		}



		public int LengthMin
		{
			get { return cbxMin.Items.Count; }
		}



		public int LengthMax
		{
			get { return cbxMax.Items.Count; }
		}

	}
}
