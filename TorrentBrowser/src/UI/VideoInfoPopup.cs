﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;


namespace TorrentBrowser
{
	public class VideoInfoPopup : Form
	{
		public const int PADDING = 20;

		private PictureBox poster;
		private readonly Panel posterContainer;
		private readonly PictureBox backdrop;
		private readonly Panel rightPanelContainer;
		private readonly VideoInfoPopupBackground rightPanel;
		private DefaultPoster defaultPoster;
		private TorrentGrid torrentGrid;
		private Video video;





		public VideoInfoPopup(Video video = null)
		{
			SuspendLayout();
			
			KeyPreview = true;
			BackColor = Color.Black;
			ClientSizeChanged += OnClientSizeChanged;

			// Create poster container
			posterContainer = new Panel();
			posterContainer.Dock = DockStyle.Left;
			Controls.Add(posterContainer);
			
			// Create right panel container
			rightPanelContainer = new Panel();
			rightPanelContainer.Dock = DockStyle.Right;
			Controls.Add(rightPanelContainer);

			// Create torrent grid
			torrentGrid = new TorrentGrid();
			rightPanelContainer.Controls.Add(torrentGrid);
			torrentGrid.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
			torrentGrid.Width = torrentGrid.Parent.Width - PADDING * 2;
			torrentGrid.Location = new Point(PADDING, torrentGrid.Parent.ClientSize.Height - PADDING - TorrentGrid.MAX_HEIGHT);

			// Create right panel
			rightPanel = new VideoInfoPopupBackground();
			rightPanel.Dock = DockStyle.Fill;
			rightPanelContainer.Controls.Add(rightPanel);

			// Create backdrop
			backdrop = new PictureBox();
			rightPanelContainer.Controls.Add(backdrop);
			backdrop.LoadCompleted += OnBackdropLoaded;
			backdrop.SizeMode = PictureBoxSizeMode.StretchImage;
			backdrop.Anchor = AnchorStyles.None;
			backdrop.Visible = false;

			ResumeLayout();

			if (video != null) {
				Video = video;
			}
		}



		public Video Video
		{
			get { return video; }

			set
			{
				if (value == video)
					return;

				video = value;

				SuspendLayout();

				Text = video != null ? video.title : "";

				torrentGrid.SetTorrents(video != null ? video.TorrentsVideos : null);
			
				rightPanel.SetVideo(video);

				// Load poster image or show default poster
				if (video != null)
				{
					if (video.hasPosterUrl)
					{
						poster = new PictureBox();
						posterContainer.Controls.Add(poster);
						poster.LoadCompleted += OnPosterLoaded;
						poster.SizeMode = PictureBoxSizeMode.StretchImage;
						poster.Anchor = AnchorStyles.None;
						poster.Visible = false;
						poster.LoadAsync(video.GetPosterUrl(PosterSize.w342.ToString()));
					}
					else
					{
						defaultPoster = new DefaultPoster();
						posterContainer.Controls.Add(defaultPoster);
						defaultPoster.SetVideo(video);
						defaultPoster.FillParent();
					}
				}

				// Load backdrop
				if (video.hasBackdropUrl) {
					backdrop.LoadAsync(video.GetBackdropUrl(BackdropSize.w780.ToString()));
				}

				ResumeLayout();
			}
		}



		private void OnPosterLoaded(object sender, AsyncCompletedEventArgs e)
		{
			poster.FillParent();
			poster.Visible = true;
		}



		private void OnBackdropLoaded(object sender, AsyncCompletedEventArgs e)
		{
			backdrop.FillParent();
			backdrop.Visible = true;
		}



		private void OnClientSizeChanged(object sender, EventArgs e)
		{
			// Poster
			posterContainer.Width = (int)(posterContainer.Height / Poster.WIDTH_TO_HEIGHT);
			poster?.FillParent();
			defaultPoster?.FillParent();

			// Backdrop
			rightPanelContainer.Width = ClientSize.Width - posterContainer.Width;
			backdrop.FillParent();

			Refresh();
		}



		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);

			switch (e.KeyCode)
			{
				case Keys.Escape:
					Close();
					break;
			}
		}



		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			poster?.CancelAsync();
			backdrop.CancelAsync();
		}

	}

}
