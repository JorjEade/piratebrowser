﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace TorrentBrowser
{
	class ContentDatabase : IDisposable
	{

		public readonly List<Content> Contents = new List<Content>();
		public event Action<Content> ContentCreated;

		private readonly List<CancellationTokenSource> taskCancellationTokens = new List<CancellationTokenSource>();
		private int numContentsBeingCreated;
		private readonly object numContentsBeingCreated_Lock = new object();

		public int NumContentsBeingCreated
		{
			get { lock (numContentsBeingCreated_Lock) return numContentsBeingCreated; }
			set { lock (numContentsBeingCreated_Lock) numContentsBeingCreated = value; }
		}

		public int NumContentsCreatedOrBeingCreated
		{
			get { return NumContentsBeingCreated + Contents.Count; }
		}



		public void AddFromTorrent(Torrent torrent)
		{
			// First, try to add the torrent to an existing content
			lock (Contents)
				if (Contents.Exists(content => content.TryAddTorrent(torrent)))
					return;

			CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

			lock (taskCancellationTokens)
				taskCancellationTokens.Add(cancellationTokenSource);

			Task.Run(
				() => _AddFromTorrent(torrent, cancellationTokenSource),
				cancellationTokenSource.Token
			);
		}



		public void _AddFromTorrent(Torrent torrent, CancellationTokenSource cancellationToken)
		{
			// Create a new content for the torrent
			++NumContentsBeingCreated;
			Content newContent = Content.CreateFromTorrent(torrent, cancellationToken);
			--NumContentsBeingCreated;
			if (newContent == null)
				goto end;

			lock (Contents)
			{
				// If the new content is the same as an existing one, add the torrent to it
				foreach (Content existingContent in Contents)
				{
					if (newContent.IsSameAs(existingContent))
					{
						existingContent.AddTorrent(torrent);
						newContent.Dispose();
						goto end;
					}
				}

				// Otherwise, add the new content
				Contents.Add(newContent);
				ContentCreated?.CrossThreadInvoke(newContent);
			}

			end:

			// Remove cancellation token
			lock (taskCancellationTokens)
				taskCancellationTokens.Remove(cancellationToken);
		}



		public void CancelLoad()
		{
			lock (taskCancellationTokens)
				taskCancellationTokens.ForEach(c => c?.Cancel());
		}



		public void ClearContents()
		{
			lock (Contents)
				Contents.DisposeAndClear();
		}



		public void Dispose()
		{
			CancelLoad();
			ClearContents();
		}
	}
}
