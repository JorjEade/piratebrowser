﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;


namespace TorrentBrowser
{
	public class TorrentGrid : DataGridView
	{
		public const int MAX_HEIGHT = 160;
		
		private const string COLUMN_SEASON =	"season";
		private const string COLUMN_EPISODE =	"episode";
		private const string COLUMN_TYPE =		"type";
		private const string COLUMN_RES =		"res";
		private const string COLUMN_AUDIO =		"audio";
		private const string COLUMN_VIDEO =		"video";
		private const string COLUMN_SIZE =		"size";
		private const string COLUMN_UPLOADED =	"uploaded";
		private const string COLUMN_UPLOADER =	"uploader";
		private const string COLUMN_SEED =		"seeders";
		private const string COLUMN_LEECH =		"leechers";
		private const string COLUMN_COMMENTS =	"comments";
		private const string COLUMN_LANG =		"language";
		private const string COLUMN_SUBS =		"subs";
		private const string COLUMN_DUB =		"dub";
		
		private readonly static DataGridViewCellStyle cellStyleEvenRow = new DataGridViewCellStyle() {
			BackColor = Color.Black,
			ForeColor = Color.FromArgb(163, 163, 163),
			SelectionBackColor = Color.FromArgb(60, 60, 60),
			SelectionForeColor = Color.White
		};
		
		private readonly static DataGridViewCellStyle cellStyleOddRow = new DataGridViewCellStyle(cellStyleEvenRow)
		{
			BackColor = Color.FromArgb(12, 12, 12)
		};
		



		public TorrentGrid(IList<Torrent_Video> torrents = null)
		{
			SuspendLayout();

			SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			ReadOnly = true;
			AllowUserToResizeRows = false;
			AllowUserToAddRows = false;
			MultiSelect = false;
			RowHeadersVisible = false;
			ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			CellBorderStyle = DataGridViewCellBorderStyle.None;
			BorderStyle = BorderStyle.FixedSingle;
			DefaultCellStyle = cellStyleEvenRow;
			AlternatingRowsDefaultCellStyle = cellStyleOddRow;
			this.MakeRightClickSelectCell();
			
			// Add columns
			const int WITDH_PADDING = 10;
			this.AddColumn(COLUMN_SEASON,	"Se",	width: 37 + WITDH_PADDING);
			this.AddColumn(COLUMN_EPISODE,	"Ep",	width: 37 + WITDH_PADDING);
			this.AddColumn(COLUMN_TYPE,		"Type",	width: 60 + WITDH_PADDING);
			this.AddColumn(COLUMN_RES,		"📺",	width: 37 + WITDH_PADDING);
			this.AddColumn(COLUMN_AUDIO,	"A",	width: 30 + WITDH_PADDING);
			this.AddColumn(COLUMN_VIDEO,	"V",	width: 30 + WITDH_PADDING);
			this.AddColumn(COLUMN_SIZE,		"📁",	width: 59 + WITDH_PADDING);
			this.AddColumn(COLUMN_UPLOADED,	"📅",	width: 60 + WITDH_PADDING);
			this.AddColumn(COLUMN_UPLOADER,	"By",	autoSizeMode: DataGridViewAutoSizeColumnMode.Fill);
			this.AddColumn(COLUMN_SEED,		"↑",	width: 30 + WITDH_PADDING);
			this.AddColumn(COLUMN_LEECH,	"↓",	width: 30 + WITDH_PADDING);
			this.AddColumn(COLUMN_COMMENTS,	"💬",	width: 23 + WITDH_PADDING);
			this.AddColumn(COLUMN_LANG,		"🔊",	width: 23 + WITDH_PADDING);
			this.AddColumn(COLUMN_SUBS,		"Sub",	width: 23 + WITDH_PADDING);
			this.AddColumn(COLUMN_DUB,		"Dub",	width: 23 + WITDH_PADDING);

			ResumeLayout();

			if (torrents != null) {
				SetTorrents(torrents);
			}
		}


		public void SetTorrents(IList<Torrent_Video> torrents)
		{
			SuspendLayout();

			Columns[COLUMN_SEASON].Visible =
			Columns[COLUMN_EPISODE].Visible = torrents[0].videoType == VideoType.TV;

			Height = ColumnHeadersHeight;

			// Add rows
			Rows.Clear();

			foreach (Torrent torrent in torrents)
			{
				Torrent_Video t = (Torrent_Video)torrent;
				int rowIndex = Rows.Add();
				Rows[rowIndex].Tag = t;
				this.SetCell(rowIndex, COLUMN_SEASON,	t.season);
				this.SetCell(rowIndex, COLUMN_EPISODE,	t.episode);
				this.SetCell(rowIndex, COLUMN_TYPE,		t.releaseType != ReleaseType.Unknown ? t.releaseType.ToString() : "");
				this.SetCell(rowIndex, COLUMN_RES,		t.resolution);
				this.SetCell(rowIndex, COLUMN_AUDIO,	t.audioEncoding == AudioEncoding.Unknown ? "" : t.audioEncoding.ToString());
				this.SetCell(rowIndex, COLUMN_VIDEO,	t.videoEncoding == VideoEncoding.Unknown ? "" : t.videoEncoding.ToString());
				this.SetCell(rowIndex, COLUMN_SIZE,		t.size);
				this.SetCell(rowIndex, COLUMN_UPLOADED,	t.uploadDate);
				this.SetCell(rowIndex, COLUMN_UPLOADER,	t.uploadedBy,
														t.isTrusted || t.isVip ? (Color?)Color.Gold : null);
				this.SetCell(rowIndex, COLUMN_SEED,		t.numSeeders, Color.PaleGreen);
				this.SetCell(rowIndex, COLUMN_LEECH,	t.numLeechers, Color.Tomato);
				this.SetCell(rowIndex, COLUMN_COMMENTS,	t.numComments,
														t.numComments == 0 ? null : (Color?)Color.SkyBlue,
														t.numComments == 0 ? null : (FontStyle?)FontStyle.Underline);
				this.SetCell(rowIndex, COLUMN_LANG,		t.language ==		Language.Unknown ? "" : t.language.ToString().ToUpper());
				this.SetCell(rowIndex, COLUMN_SUBS,		t.subsLanguage ==	Language.Unknown ? "" : t.subsLanguage.ToString().ToUpper());
				this.SetCell(rowIndex, COLUMN_DUB,		t.dubLanguage ==	Language.Unknown ? "" : t.dubLanguage.ToString().ToUpper());

				// Context menu
				Rows[rowIndex].ContextMenuStrip = new ContextMenuStrip();
				Rows[rowIndex].ContextMenuStrip.Items.Add(t.rawTitle).Enabled = false;
				Rows[rowIndex].ContextMenuStrip.Items.Add("Go To Page", null, (o, e) => t.OpenPageLink());
				Rows[rowIndex].ContextMenuStrip.Items.Add(new BlockUnblockUploaderContextMenuItem(t));

				// Tooltip
				foreach (DataGridViewCell cell in Rows[rowIndex].Cells)
				{
					cell.ToolTipText = t.rawTitle;

					// Override default tooltip for certain columns
					switch (cell.OwningColumn.Name)
					{
						case COLUMN_TYPE:
							if (t.releaseType != ReleaseType.Unknown) {
								cell.ToolTipText = t.releaseSubType;
							}
							break;

						case COLUMN_UPLOADED:
							cell.ToolTipText = t.uploadDate.ToString("d MMM yyyy" + (t.uploadDate.Second == 0 && t.uploadDate.Minute == 0 && t.uploadDate.Hour == 0 ? "" : " HH:mm"));
							break;

						case COLUMN_UPLOADER:
							if (t.isVip)		cell.ToolTipText = "VIP";
							if (t.isTrusted)	cell.ToolTipText = "Trusted";
							break;
					}
				}

				// Expand grid height
				if (Height + Rows[rowIndex].Height <= MAX_HEIGHT) {
					Height += Rows[rowIndex].Height;
				}
			}

			Height += 2;

			this.SetFirstSortDirectionToDesc();

			ResumeLayout();
		}



		private class BlockUnblockUploaderContextMenuItem : ToolStripMenuItem
		{
			private readonly Torrent_Video torrent;

			public BlockUnblockUploaderContextMenuItem(Torrent_Video torrent)
			{
				this.torrent = torrent;
			}

			protected override void OnClick(EventArgs e)
			{
				Main.Instance.SetUploaderBlocked(
					torrent.uploadedBy,
					!Main.Instance.IsUploaderBlocked(torrent.uploadedBy)
				);
			}

			public override string Text
			{
				get
				{
					return Main.Instance.IsUploaderBlocked(torrent.uploadedBy)
						? "Unblock Uploader"
						: "Block Uploader";
				}
			}
		}



		protected override void OnCellClick(DataGridViewCellEventArgs e)
		{
			if (e.RowIndex < 0) {
				return;
			}

			switch (Columns[e.ColumnIndex].Name)
			{
				case COLUMN_COMMENTS:
					// Open comments link
					if (((Torrent_Video)Rows[e.RowIndex].Tag).numComments > 0) {
						((Torrent_Video)Rows[e.RowIndex].Tag).OpenCommentsLink();
					}
					break;
			}
		}



		protected override void OnCellDoubleClick(DataGridViewCellEventArgs e)
		{
			if (e.RowIndex < 0) {
				return;
			}

			switch (Columns[e.ColumnIndex].Name)
			{
				case COLUMN_COMMENTS:
					break;

				default:
					// Open magnet link
					((Torrent_Video)Rows[e.RowIndex].Tag).OpenMagnetLink();
					break;
			}
		}



		protected override void OnCellFormatting(DataGridViewCellFormattingEventArgs e)
		{
			if (e.RowIndex < 0) {
				return;
			}

			Torrent_Video t = (Torrent_Video)Rows[e.RowIndex].Tag;

			switch (Columns[e.ColumnIndex].Name)
			{
				case COLUMN_SEASON:
					if (!(e.Value is int)) break;
					e.Value = t.season == 0 ? "" : t.season + (t.seasonMax > t.season ? "-" + t.seasonMax : "");
					e.FormattingApplied = true;
					break;

				case COLUMN_EPISODE:
					if (!(e.Value is int)) break;
					e.Value = t.episode == 0 ? "" : t.episode + (t.episodeMax > t.episode ? "-" + t.episodeMax : "");
					e.FormattingApplied = true;
					break;

				case COLUMN_RES:
					if (!(e.Value is int)) break;
					e.Value = StringUtils.ResolutionString((int)e.Value);
					e.FormattingApplied = true;
					break;

				case COLUMN_SIZE:
					if (!(e.Value is ulong)) break;
					e.Value = StringUtils.FileSizeString((ulong)e.Value);
					e.FormattingApplied = true;
					break;

				case COLUMN_UPLOADED:
					if (!(e.Value is DateTime)) break;
					e.Value = t.uploadDate.Age(beSpecificIfUnder24Hours: true);
					e.FormattingApplied = true;
					break;
			}
		}
	}
}
