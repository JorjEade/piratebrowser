﻿using System;
using System.Drawing;
using System.Windows.Forms;


public class Curtain : TransparentDrawingArea
{

	public double MaxOpacity { get; set; } = 1;
	public double FadeDuration { get; set; } = 1;
	public int FPS { get; set; } = 60;
		
	public double Opacity
	{
		get { return opacity * MaxOpacity; }

		set
		{
			opacity = value * MaxOpacity;
			Visible = opacity > 0;
			Parent?.Refresh();
		}
	}

	private SolidBrush brush = new SolidBrush(Color.Black);
	private double opacity;
	private Animation.Token animationToken;



	public Curtain()
	{
		Dock = DockStyle.Fill;
	}



	public void FadeOut(double? from = null, Action onComplete = null)
	{
		animationToken?.Cancel();

		animationToken = Animation.Tween(
			onUpdate:	val => Opacity = val,
			from:		from ?? Opacity,
			to:			0,
			duration:	FadeDuration,
			fps:		FPS,
			onComplete:	onComplete
		);
	}



	public void FadeIn(double? from = null, Action onComplete = null)
	{
		animationToken?.Cancel();

		animationToken = Animation.Tween(
			onUpdate:	val => Opacity = val,
			from:		from ?? Opacity,
			to:			1,
			duration:	FadeDuration,
			fps:		FPS,
			onComplete:	onComplete
		);
	}



	protected override void OnDraw()
	{
		brush.Color = Color.FromArgb((int)(Opacity * 255), Color.Black);
		graphics.FillRectangle(brush, ClientRectangle);
	}



	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
			
		animationToken?.Cancel();
		brush.Dispose();
	}

}
