﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;


namespace TorrentBrowser
{
	class PosterSummaryOverlay : TransparentDrawingArea
	{

		private const float SHADE_HEIGHT_TOP_PERCENT = 0.45f;
		private const float SHADE_HEIGHT_BOTTOM_PERCENT = 0.45f;


		private static readonly StringFormat stringFormatTitle = new StringFormat() { Alignment = StringAlignment.Center };
		private static readonly StringFormat stringFormatBlurb = new StringFormat() { LineAlignment = StringAlignment.Far };
		private static readonly SolidBrush whiteBrush = new SolidBrush(Color.White);
		private static readonly Font fontTitle = new Font("Arial", 20);
		private static readonly Font fontBlurb = new Font("Arial", 12);

		private Video video;
		private StarDisplay starDisplay;



		public PosterSummaryOverlay(Video video = null)
		{
			starDisplay = new StarDisplay();
			starDisplay.showPercent = false;
			Controls.Add(starDisplay);

			if (video != null) {
				SetVideo(video);
			}
		}



		public void SetVideo(Video video)
		{
			this.video = video;

			if (starDisplay.Visible = video.normScore > 0) {
				starDisplay.normScore = video.normScore;
			}
		}



		protected override void OnSizeChanged(EventArgs e)
		{
			starDisplay.Location = new Point(
				(Width - starDisplay.StarsArea.Width) / 2,
				(Height - starDisplay.StarsArea.Height) - 25
			);
		}



		protected override void OnDraw()
		{
			int shadeHeightBottom = (int)(Height * SHADE_HEIGHT_BOTTOM_PERCENT);

			using (Brush brushShadeBottom = new LinearGradientBrush(new Point(0, Height), new Point(0, Height - shadeHeightBottom), Color.Black, Color.Transparent))
			{
				graphics.FillRectangle(
					brushShadeBottom,
					0,
					Height - shadeHeightBottom,
					Width,
					shadeHeightBottom
				);
			}
		}
	}
}
