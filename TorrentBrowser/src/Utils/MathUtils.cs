﻿using System;


public static class MathUtils
{

	public static double Lerp(double from, double to, double t)
	{
		return from + (to - from) * t;
	}


	public static double Clamp01(double t)
	{
		return Math.Min(Math.Max(0, t), 1);
	}

}
