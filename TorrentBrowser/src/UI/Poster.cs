﻿using System;
using System.Windows.Forms;
using System.ComponentModel;


namespace TorrentBrowser
{
	class Poster : Panel
	{

		public const float WIDTH_TO_HEIGHT = 450f / 300f;

		public event Action<Poster> Clicked;
		public Video Video { get; private set; }

		private readonly PictureBox pictureBox;
		private readonly InvisibleButton button;
		private PosterSummaryOverlay summaryOverlay;
		private Curtain curtain_FadeIn;
		private Curtain curtain_Dim;
		
		

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams cp = base.CreateParams;
				cp.ExStyle |= 0x02000000; // Turn on WS_EX_COMPOSITED
				return cp;
			}
		}


		public Poster(int width, int height, Video video)
		{
			Video = video;
			Width = width;
			Height = height;
			Margin = new Padding(0);

			// Create invisible button
			button = new InvisibleButton();
			Controls.Add(button);
			button.Size = Size;
			button.MouseClick += OnClick;
			button.MouseEnter += OnMouseEnter;
			button.MouseLeave += OnMouseLeave;

			// Create "fade in" curtain
			curtain_FadeIn = new Curtain();
			curtain_FadeIn.Opacity = 1;
			curtain_FadeIn.FadeDuration = 0.5;
			Controls.Add(curtain_FadeIn);

			// Create "background dim" curtain
			curtain_Dim = new Curtain();
			curtain_Dim.Opacity = 0.85;
			curtain_Dim.Visible = false;
			Controls.Add(curtain_Dim);

			// Create summary overlay
			summaryOverlay = new PosterSummaryOverlay(video);
			summaryOverlay.Dock = DockStyle.Fill;
			summaryOverlay.Visible = false;
			Controls.Add(summaryOverlay);

			// Create picture box
			pictureBox = new PictureBox();
			Controls.Add(pictureBox);
			pictureBox.LoadCompleted += OnImageLoaded;
			pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
			pictureBox.Anchor = AnchorStyles.None;
			pictureBox.InitialImage = null;
			pictureBox.Visible = false;

			// Load poster image or show default poster
			if (video.hasPosterUrl) {
				pictureBox.LoadAsync(video.GetPosterUrl(PosterSize.w342.ToString()));
			}
			else {
				DefaultPoster defaultPoster = new DefaultPoster(video);
				defaultPoster.Dock = DockStyle.Fill;
				Controls.Add(defaultPoster);
				Reveal();
			}
		}



		private void OnImageLoaded(object sender, AsyncCompletedEventArgs e)
		{
			pictureBox.FillParent();
			pictureBox.Visible = true;
			Reveal();
		}



		private void Reveal()
		{
			curtain_FadeIn.FadeOut();
		}



		private void OnMouseEnter(object sender, EventArgs e)
		{
			summaryOverlay.Visible = true;
		}



		private void OnMouseLeave(object sender, EventArgs e)
		{
			summaryOverlay.Visible = false;
		}



		private void OnClick(object sender, MouseEventArgs e)
		{
			switch (e.Button)
			{
				case MouseButtons.Left:
					Clicked?.Invoke(this);
					break;
			}
		}



		public bool Dimmed
		{
			get { return curtain_Dim.Visible; }

			set
			{
				curtain_Dim.Visible = value;
				if (value) summaryOverlay.Visible = false;
			}
		}



		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			pictureBox.CancelAsync();
		}

	}
}
