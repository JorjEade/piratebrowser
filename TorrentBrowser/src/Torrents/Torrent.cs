﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;


namespace TorrentBrowser
{
	public class Torrent : IComparable<Torrent>
	{

		public string IdForContent { get; protected set; }


		public readonly int category;
		public readonly int subcategory;
		public readonly string pageUrl;
		public readonly string commentsUrl;
		public readonly string rawTitle;
		public string cleanTitle;
		public readonly string magnetUri;
		public readonly int numComments;
		public readonly bool isTrusted;
		public readonly bool isVip;
		public readonly DateTime uploadDate;
		public readonly ulong size;
		public readonly string uploadedBy;
		public readonly string uploadedByTrustedVip;
		public readonly int numSeeders;
		public readonly int numLeechers;



		public Torrent(int category, int subcategory, string pageUrl, string rawTitle, string magnetLink, int numComments, bool isTrusted, bool isVip, DateTime uploadDate, ulong size, string uploadedBy, int numSeeders, int numLeechers)
		{
			this.category = category;
			this.subcategory = subcategory;
			this.pageUrl = pageUrl;
			this.commentsUrl = pageUrl + "#comments";
			this.rawTitle = rawTitle;
			this.magnetUri = magnetLink;
			this.numComments = numComments;
			this.isTrusted = isTrusted;
			this.isVip = isVip;
			this.uploadDate = uploadDate;
			this.size = size;
			this.uploadedBy = uploadedBy;
			this.uploadedByTrustedVip = uploadedBy + (isTrusted ? " (Trusted)" : "") + (isVip ? " (VIP)" : "");
			this.numSeeders = numSeeders;
			this.numLeechers = numLeechers;
			
			// Clean up title
			cleanTitle = rawTitle;//.ToLower();
			cleanTitle = StringUtils.StripTags(cleanTitle);
			cleanTitle = Regex.Replace(cleanTitle, @"^18 ?\+", "");		// Remove 18+ from start			
			cleanTitle = Regex.Replace(cleanTitle, @"&|\+", "and");		// Replace "&" and "+" with "and"
			cleanTitle = Regex.Replace(cleanTitle, @"’|'", "");			// Remove unnecessary grammar
			cleanTitle = Regex.Replace(cleanTitle, @"\[|\{\(", " (");	// Replace open brackets with " ("
			cleanTitle = Regex.Replace(cleanTitle, @"\]|\}\)", ") ");	// Replace close brackets with ") "
			
			// Move anything in brackets from start to end
			while (true) {
				Match m = Regex.Match(cleanTitle, @"^\s*\(.*?\)\s*");
				if (!m.Success) break;
				cleanTitle = cleanTitle.Substring(m.Length) + m.Groups[0];
			}

			cleanTitle = Regex.Replace(cleanTitle, @"[^\s\d\p{L}\(\)\-]", " ");		// Replace anything that isn't a (space, digit, letter, bracket, hyphen) with a space
			cleanTitle = StringUtils.NormaliseWhite(cleanTitle);

			IdForContent = cleanTitle;
		}



		public void OpenMagnetLink()
		{
			Process.Start(magnetUri);
		}



		public void OpenPageLink()
		{
			Process.Start(pageUrl);
		}



		public void OpenCommentsLink()
		{
			Process.Start(commentsUrl);
		}



		public TimeSpan Age
		{
			get { return DateTime.Now - uploadDate; }
		}



		public override string ToString()
		{
			return rawTitle;
		}



		public virtual int CompareTo(Torrent other)
		{
			return Math.Sign(other.numSeeders - numSeeders);
		}

	}

}
