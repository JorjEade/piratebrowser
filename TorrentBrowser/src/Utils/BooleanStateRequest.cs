﻿using System;
using System.Collections.Generic;


class BooleanStateRequest
{

	private readonly List<object> sources = new List<object>();

	public bool Value { get; private set; }
	public event Action<bool> onStateChanged;

	private bool defaultValue;



	public BooleanStateRequest(bool defaultValue = false, Action<bool> onStateChanged = null)
	{
		this.defaultValue = defaultValue;
		this.onStateChanged = onStateChanged;
	}



	public void AddContraryStateRequest(object source)
	{
		if (sources.Contains(source))
			return;

		sources.Add(source);

		if (sources.Count == 1) {
			Value = !defaultValue;
			onStateChanged?.CrossThreadInvoke(Value);
		}
	}



	public void RemoveContraryStateRequest(object source)
	{
		if (!sources.Contains(source))
			return;

		sources.Remove(source);

		if (sources.Count == 0) {
			Value = defaultValue;
			onStateChanged?.CrossThreadInvoke(Value);
		}
	}



	public static implicit operator bool(BooleanStateRequest targ)
	{
		return targ.Value;
	}

}
