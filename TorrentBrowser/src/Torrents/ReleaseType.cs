﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;


namespace TorrentBrowser
{
	public enum ReleaseType
	{
		Unknown,
		Cam,
		Telesync,
		Workprint,
		Telecine,
		PPVRip,
		Screener,
		DDC,
		R5,
		DVDRip,
		DVDR,
		TVRip,
		VODRip,
		WebDL,
		WebRip,
		WebCap,
		BluRay
	};

	public static class ReleaseTypeUtils
	{
		
		private static readonly StringBuilder sb = new StringBuilder();
		private static readonly Dictionary<ReleaseType, string> regexes = new Dictionary<ReleaseType, string>();
		private static readonly Dictionary<ReleaseType, string[]> subTypes = new Dictionary<ReleaseType, string[]>()
		{
			{ ReleaseType.TVRip,		new string[] { "dsr", "ds rip", "sat rip", "dth rip", "dvb rip", "hd tv", "pd tv", "tv rip", "hd tv rip" } },
			{ ReleaseType.WebDL,		new string[] { "web dl", "hd rip", "dl rip", "web dl rip" } },
			{ ReleaseType.Telesync,		new string[] { "ts", "hd ts", "telesync", "p dvd", "pre dvd rip", "ts rip", "hd ts rip" } },
			{ ReleaseType.BluRay,		new string[] { "blu ray", "bd rip", "br rip", "bd mv", "bdr", "bd 5", "bd 9", "bd 25", "bd 50", "bd", "blue ray" } },
			{ ReleaseType.Screener,		new string[] { "scr", "screener", "dvd scr", "dvd screener", "bd scr" } },
			{ ReleaseType.R5,			new string[] { "r5", "r5 line", "r5 ac3 5 1 hq" } },
			{ ReleaseType.DVDRip,		new string[] { "dvd rip", "dvd mux" } },
			{ ReleaseType.DVDR,			new string[] { "dvd r", "dvd full", "full rip", "iso rip", "lossless rip", "untouched rip", "dvd 5", "dvd 9", "dvd" } },
			{ ReleaseType.Cam,			new string[] { "cam", "cam rip", "hd cam", "hq cam" } },
			{ ReleaseType.WebRip,		new string[] { "web", "web rip" } },
			{ ReleaseType.WebCap,		new string[] { "web cap" } },
			{ ReleaseType.DDC,			new string[] { "ddc" } },
			{ ReleaseType.PPVRip,		new string[] { "ppv", "ppv rip" } },
			{ ReleaseType.VODRip,		new string[] { "vod r", "vod rip" } },
			{ ReleaseType.Telecine,		new string[] { "tc", "hd tc", "tele cine" } },
			{ ReleaseType.Workprint,	new string[] { "wp", "workprint" } }
		};

		public static readonly ReleaseType[] qualityReleaseTypes = new ReleaseType[]
		{
			ReleaseType.Unknown,
			ReleaseType.PPVRip,
			ReleaseType.DVDRip,
			ReleaseType.DVDR,
			ReleaseType.TVRip,
			ReleaseType.VODRip,
			ReleaseType.WebDL,
			ReleaseType.WebRip,
			ReleaseType.WebCap,
			ReleaseType.BluRay
		};



		static ReleaseTypeUtils()
		{
			// Create regexes
			foreach (KeyValuePair<ReleaseType, string[]> kvp in subTypes)
			{
				sb.Clear();
				sb.Append(@"(?:\s|\()(");
				foreach (string label in kvp.Value) {
					sb.Append(label.Replace(" ", @"\s?"));
					sb.Append("|");
				}
				sb.Remove(sb.Length - 1, 1);
				sb.Append(@")(?:\s|$|\))");
				regexes.Add(kvp.Key, sb.ToString());
			}
		}



		public static ReleaseType GetReleaseType(string input, out int index, out string subtype)
		{
			index = -1;
			subtype = "";

			foreach (KeyValuePair<ReleaseType, string> kvp in regexes)
			{
				Match match = Regex.Match(input, kvp.Value, RegexOptions.IgnoreCase);

				if (match.Success)
				{
					index = match.Groups[1].Index;
					subtype = match.Groups[1].Value;
					return kvp.Key;
				}
			}

			return ReleaseType.Unknown;
		}

		
	}
}
