﻿using System;
using System.Collections.Generic;


namespace TorrentBrowser
{
	public class TorrentFilter_Video : TorrentFilter
	{

		public string title;
		public int year;
		public int season;
		public int episode;
		public int resolutionMin = 0;
		public int resolutionMax = int.MaxValue;
		public ulong sizeMin = 0;
		public ulong sizeMax = ulong.MaxValue;
		public ReleaseType[] releaseTypes;
		public VideoEncoding[] videoEncoding;
		public AudioEncoding[] audioEncoding;
		public TimeSpan ageMin = TimeSpan.MinValue;
		public TimeSpan ageMax = TimeSpan.MaxValue;
		public int minSeedLeech = 0;
		public IList<string> uploaderInclude;
		public IList<string> uploaderExclude;
		public bool hasComments;
		public bool notHc;
		public bool vip;
		public bool trusted;
		public VideoType? videoType;
		
		

		public override bool MeetsCriteria(Torrent torrent)
		{
			Torrent_Video video = torrent as Torrent_Video;

			if (video == null) {
				throw new InvalidCastException();
			}
			
			return
				base.MeetsCriteria(torrent) &&
				(string.IsNullOrEmpty(title) || video.title.ToLower().Contains(title.ToLower())) &&
				(year == 0 || video.releaseDate.Year == year) &&
				(season == 0 || (season >= video.season && season <= video.seasonMax)) &&
				(episode == 0 || (episode >= video.episode && episode <= video.episodeMax)) &&
				(video.resolution >= resolutionMin) &&
				(video.resolution <= resolutionMax) &&
				(video.size >= sizeMin) &&
				(video.size <= sizeMax) &&
				(releaseTypes == null || Array.IndexOf(releaseTypes, video.releaseType) != -1) &&
				(videoEncoding == null || Array.IndexOf(videoEncoding, video.videoEncoding) != -1) &&
				(audioEncoding == null || Array.IndexOf(audioEncoding, video.audioEncoding) != -1) &&
				(video.Age >= ageMin) &&
				(video.Age <= ageMax) &&
				(video.numSeeders + video.numLeechers >= minSeedLeech) &&
				(uploaderInclude == null || uploaderInclude.Count == 0 || uploaderInclude.Contains_IgnoreCase(video.uploadedBy)) &&
				(uploaderExclude == null || uploaderExclude.Count == 0 || !uploaderExclude.Contains_IgnoreCase(video.uploadedBy)) &&
				(!hasComments || video.numComments > 0) &&
				(!notHc || !video.isHc) &&
				(!vip || video.isVip) &&
				(!trusted || video.isTrusted) &&
				(!videoType.HasValue || video.videoType == videoType.Value);
		}

	}

}
