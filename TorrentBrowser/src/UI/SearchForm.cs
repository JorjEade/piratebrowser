﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;


namespace TorrentBrowser
{
	public class SearchForm : Form
	{

		private static readonly ulong[] fileSizes = new ulong[]
		{
			(ulong)Utils.MBtoBytes(128),
			(ulong)Utils.MBtoBytes(256),
			(ulong)Utils.MBtoBytes(512),
			(ulong)Utils.GBtoBytes(1),
			(ulong)Utils.GBtoBytes(2),
			(ulong)Utils.GBtoBytes(3),
			(ulong)Utils.GBtoBytes(4),
			(ulong)Utils.GBtoBytes(5),
			(ulong)Utils.GBtoBytes(6),
			(ulong)Utils.GBtoBytes(7),
			(ulong)Utils.GBtoBytes(8),
			(ulong)Utils.GBtoBytes(9),
			(ulong)Utils.GBtoBytes(10),
		};

		private static readonly TimeSpan[] ages = new TimeSpan[]
		{
			TimeSpan.FromDays(0),
			TimeSpan.FromDays(1),
			TimeSpan.FromDays(7),
			TimeSpan.FromDays(30),
			TimeSpan.FromDays(365 * 1),
			TimeSpan.FromDays(365 * 2),
			TimeSpan.FromDays(365 * 3),
			TimeSpan.FromDays(365 * 4),
			TimeSpan.FromDays(365 * 5),
			TimeSpan.FromDays(365 * 6),
			TimeSpan.FromDays(365 * 7),
			TimeSpan.FromDays(365 * 8),
			TimeSpan.FromDays(365 * 9),
			TimeSpan.FromDays(365 * 10)
		};

		private static readonly int[] minSeedsLeeches = new int[]
		{
			1,
			10,
			50,
			100,
			500,
			1000,
			5000,
			10000,
		};

		private static readonly Dictionary<string, int> resolutions = new Dictionary<string, int>()
		{
			{ "480p",	480 },
			{ "540p",	540 },
			{ "576p",	576 },
			{ "720p",	720 },
			{ "1080p",	1080 },
			{ "4K",		2160 },
			{ "8K",		4320 },
		};

		public event Action<SearchForm> Search;
		public event Action<SearchForm> Hidden;

		private RadioButton rdbMovie;
		private RadioButton rdbTv;
		private TextBox tbxSearchQuery;
		private Button btnSearch;
		private Panel pnlYear;
		private TextBox tbxYear;
		private Panel pnlEpisode;
		private Panel pnlSeasonEpisode;
		private TextBox tbxSeason;
		private TextBox tbxEpisode;
		private Range<int> rngResolution;
		private Range<ulong> rngFileSize;
		private DropDownCheckList chlVideoEncoding;
		private DropDownCheckList chlAudioEncoding;
		private Range<TimeSpan> rngAge;
		private ComboBox cbxSL;
		private TextBox tbxUploaderInclude;
		private TextBox tbxUploaderExclude;
		private CheckBox chbHasComments;
		private CheckBox chbVip;
		private CheckBox chbTrusted;
		private DropDownCheckList chlReleaseTypes;
		private CheckBox chbNotHc;



		public SearchForm()
		{
			// Initialise form
			Visible = false;
			Text = "Search";
			Padding = new Padding(10, 10, 0, 5);
			AutoSize = true;
			KeyPreview = true;
			FormBorderStyle = FormBorderStyle.FixedSingle;
			MaximizeBox = false;
			MinimizeBox = false;
			ResumeLayout();
			
			int y;
			int labelsColWidth;
			


			// ------------ FILE ------------

			// Group box
			GroupBox groupBox_File = new GroupBox();
			Controls.Add(groupBox_File);
			groupBox_File.Location = new Point(Padding.Left, Padding.Top + 80);
			groupBox_File.Size = new Size(210, 185);
			groupBox_File.Text = "File";
			groupBox_File.Padding = new Padding(1, 20, 10, 10);

			y = 0;
			labelsColWidth = 52;

			// Resolution
			rngResolution = new Range<int>();
			rngResolution.AddToMin(new ComboBoxItem<int>("No Min", 0));
			foreach (KeyValuePair<string, int> kvp in resolutions) {
				rngResolution.Add(new ComboBoxItem<int>(kvp.Key, kvp.Value));
			}
			rngResolution.AddToMax(new ComboBoxItem<int>("No Max", int.MaxValue));
			AddControlWithLabel(groupBox_File, "Res", labelsColWidth, ref y, rngResolution);

			// Size
			rngFileSize = new Range<ulong>();
			rngFileSize.AddToMin(new ComboBoxItem<ulong>("No Min", 0));
			foreach (ulong size in fileSizes) {
				rngFileSize.Add(new ComboBoxItem<ulong>(StringUtils.FileSizeString(size), size));
			}
			rngFileSize.AddToMax(new ComboBoxItem<ulong>("No Max", ulong.MaxValue));
			AddControlWithLabel(groupBox_File, "Size", labelsColWidth, ref y, rngFileSize);

			// Video encoding
			chlVideoEncoding = new DropDownCheckList();
			chlVideoEncoding.AddEnum(typeof(VideoEncoding));
			AddControlWithLabel(groupBox_File, "Video", labelsColWidth, ref y, chlVideoEncoding);

			// Audio encoding
			chlAudioEncoding = new DropDownCheckList();
			chlAudioEncoding.AddEnum(typeof(AudioEncoding));
			AddControlWithLabel(groupBox_File, "Audio", labelsColWidth, ref y, chlAudioEncoding);

			// Release types
			chlReleaseTypes = new DropDownCheckList();
			chlReleaseTypes.AddEnum(typeof(ReleaseType));
			AddControlWithLabel(groupBox_File, "Types", labelsColWidth, ref y, chlReleaseTypes);

			// Not HC
			chbNotHc = new CheckBox();
			AddControlWithLabel(groupBox_File, "Not HC", labelsColWidth, ref y, chbNotHc);
			


			// ------------ TORRENT ------------

			y = 0;
			labelsColWidth = 65;

			// Group box
			GroupBox groupBox_Torrent = new GroupBox();
			Controls.Add(groupBox_Torrent);
			groupBox_Torrent.Location = new Point(Padding.Left + groupBox_File.Width + Padding.Left, groupBox_File.Location.Y);
			groupBox_Torrent.Size = new Size(225, groupBox_File.Size.Height);
			groupBox_Torrent.Text = "Torrent";
			groupBox_Torrent.Padding = new Padding(1, 20, 10, 10);

			// Age
			rngAge = new Range<TimeSpan>();
			foreach (TimeSpan age in ages) {
				rngAge.Add(new ComboBoxItem<TimeSpan>(age.Age(beSpecificIfUnder24Hours: false), age));
			}
			rngAge.AddToMax(new ComboBoxItem<TimeSpan>("No Max", TimeSpan.MaxValue));
			AddControlWithLabel(groupBox_Torrent, "Age", labelsColWidth, ref y, rngAge);

			// S/L
			cbxSL = new ComboBox();
			cbxSL.DropDownStyle = ComboBoxStyle.DropDownList;
			cbxSL.Items.Add(new ComboBoxItem<int>("No Min", 0));
			foreach (int x in minSeedsLeeches) {
				cbxSL.Items.Add(new ComboBoxItem<int>(x + " or more", x));
			}
			AddControlWithLabel(groupBox_Torrent, "S/L", labelsColWidth, ref y, cbxSL);

			// Uploader
			tbxUploaderInclude = new TextBox() { BackColor = Color.FromArgb(240, 255, 240) };
			tbxUploaderExclude = new TextBox() { BackColor = Color.FromArgb(255, 240, 240) };
			AddControlWithLabel(groupBox_Torrent, "Uploader", labelsColWidth, ref y, tbxUploaderInclude, tbxUploaderExclude);
			y -= 3;

			// VIP
			chbVip = new CheckBox();
			AddControlWithLabel(groupBox_Torrent, "VIP", labelsColWidth, ref y, chbVip);

			// Trusted
			chbTrusted = new CheckBox();
			AddControlWithLabel(groupBox_Torrent, "Trusted", labelsColWidth, ref y, chbTrusted);

			// Has comments
			chbHasComments = new CheckBox();
			AddControlWithLabel(groupBox_Torrent, "Comments", labelsColWidth, ref y, chbHasComments);



			// ------------ HEADER ------------

			y = 0;
			labelsColWidth = 50;

			// Query
			tbxSearchQuery = new TextBox();
			AddControlWithLabel(this, "Title", labelsColWidth, ref y, tbxSearchQuery);
			tbxSearchQuery.Width -= 100;
			ActiveControl = tbxSearchQuery;
			
			// Content type
			rdbMovie = new RadioButton() { Text = "Movie" };
			rdbMovie.CheckedChanged += RbMovie_CheckedChanged;
			rdbTv = new RadioButton() { Text = "TV" };
			rdbTv.CheckedChanged += RbTv_CheckedChanged;
			AddControlWithLabel(this, "Type", labelsColWidth, ref y, rdbMovie, rdbTv);
			rdbMovie.Width = rdbTv.Width = 55;
			
			labelsColWidth = 55;

			// Year
			y = 0;
			pnlYear = new Panel() { Bounds = new Rectangle(rdbMovie.Bounds.Right, rdbMovie.Location.Y, 90, rdbMovie.Height) };
			Controls.Add(pnlYear);
			tbxYear = new TextBox();
			tbxYear.MaxLength = 4;
			tbxYear.RestrictToNumbers();
			AddControlWithLabel(pnlYear, "Year", labelsColWidth, ref y, tbxYear);
			pnlYear.Enabled = false;


			pnlSeasonEpisode = new Panel() { Bounds = new Rectangle(rdbTv.Bounds.Right, rdbTv.Location.Y, pnlYear.Width * 2, rdbTv.Height) };
			Controls.Add(pnlSeasonEpisode);
			pnlSeasonEpisode.Enabled = false;

			// Season
			y = 0;
			Panel p = new Panel() { Size = new Size(pnlYear.Width, rdbTv.Height) };
			pnlSeasonEpisode.Controls.Add(p);
			tbxSeason = new TextBox();
			tbxSeason.MaxLength = 2;
			tbxSeason.RestrictToNumbers();
			tbxSeason.TextChanged += (s, e) => pnlEpisode.Enabled = !string.IsNullOrEmpty(tbxSeason.Text);
			AddControlWithLabel(p, "Season", labelsColWidth, ref y, tbxSeason);

			// Episode
			y = 0;
			pnlEpisode = new Panel() { Bounds = new Rectangle(p.Width, 0, p.Width, p.Height) };
			pnlSeasonEpisode.Controls.Add(pnlEpisode);
			tbxEpisode = new TextBox();
			tbxEpisode.MaxLength = 2;
			tbxEpisode.RestrictToNumbers();
			AddControlWithLabel(pnlEpisode, "Episode", labelsColWidth, ref y, tbxEpisode);
			pnlEpisode.Enabled = false;
			
			// Search button
			btnSearch = new Button();
			btnSearch.Text = "Search";
			btnSearch.Click += SearchButton_Click;
			btnSearch.Location = new Point(tbxSearchQuery.Bounds.Right + 5, tbxSearchQuery.Location.Y - 1);
			btnSearch.Size = new Size(groupBox_Torrent.Bounds.Right - btnSearch.Location.X, tbxSearchQuery.Height + 2);
			btnSearch.DialogResult = DialogResult.OK;
			Controls.Add(btnSearch);
			AcceptButton = btnSearch;


			LoadDefaultSettings2();
		}



		public new void Show(IWin32Window owner)
		{
			base.Show(owner);
			tbxUploaderExclude.Text = string.Join(",", Main.excludedUploaders);
		}



		private void LoadDefaultSettings()
		{
			rdbMovie.Checked = true;
			rngResolution.IndexMin = 0;
			rngResolution.IndexMax = rngResolution.LengthMax - 1;
			rngFileSize.IndexMin = 0;
			rngFileSize.IndexMax = rngFileSize.LengthMax - 1;
			chlVideoEncoding.CheckAll();
			chlAudioEncoding.CheckAll();
			chlReleaseTypes.CheckAll();
			chbNotHc.Checked = false;
			rngAge.IndexMin = 0;
			rngAge.IndexMax = rngAge.LengthMax - 1;
			cbxSL.SelectedIndex = 0;
			tbxUploaderInclude.Text = "";
			tbxUploaderExclude.Text = "";
			chbHasComments.Checked = false;
			chbVip.Checked = false;
			chbTrusted.Checked = false;
		}



		private void LoadDefaultSettings2()
		{
			rdbMovie.Checked = true;
			rngResolution.ValueMin = 720;
			rngResolution.IndexMax = rngResolution.LengthMax - 1;
			rngFileSize.IndexMin = 0;
			rngFileSize.IndexMax = rngFileSize.LengthMax - 1;
			chlVideoEncoding.CheckAll();
			chlAudioEncoding.CheckAll();
			chlReleaseTypes.SetCheckedItems(ReleaseTypeUtils.qualityReleaseTypes);
			chbNotHc.Checked = true;
			rngAge.IndexMin = 0;
			rngAge.IndexMax = rngAge.LengthMax - 1;
			cbxSL.SetSelectedValue(1);
			tbxUploaderInclude.Text = "";
			tbxUploaderExclude.Text = string.Join(",", Main.excludedUploaders);
			chbHasComments.Checked = false;
			chbVip.Checked = false;
			chbTrusted.Checked = false;
		}



		private Label AddControlWithLabel(Control parent, string labelText, int labelsColumnWidth, ref int prevPanelY, params Control[] controls)
		{
			const int ROW_HEIGHT = 20;
			const int ROW_SPACING = 2;
			const int ROW_SPACING_2 = 5;

			// Label
			Label label = new Label();
			parent.Controls.Add(label);
			label.Text = labelText + ":";
			label.Size = new Size(labelsColumnWidth - 3, ROW_HEIGHT);
			label.Location = new Point(
				parent.Padding.Left,
				parent.Padding.Top + prevPanelY + 3
			);
			label.TextAlign = ContentAlignment.TopRight;


			// Controls
			for (int i = 0; i < controls.Length; ++i)
			{
				Control control = controls[i];
				parent.Controls.Add(control);
				control.Size = new Size(
					parent.ClientSize.Width - labelsColumnWidth - parent.Padding.Left - parent.Padding.Right,
					ROW_HEIGHT
				);
				control.Location = new Point(
					parent.Padding.Left + labelsColumnWidth,
					parent.Padding.Top + prevPanelY
				);
				prevPanelY += (control is CheckBox ? 13 : 20) + ROW_SPACING;
			}

			prevPanelY += ROW_SPACING_2;

			return label;
		}



		public string[] GetSearchUrls()
		{
			string[] ret = new string[Categories.Length];

			if (string.IsNullOrEmpty(SearchQuery))
			{
				// If no search query entered, just browse the selected category
				for (int i = ret.Length - 1; i >= 0; --i)
					ret[i] = PirateSearch.GetPath_BrowseCategory(Categories[i]);
			}
			else
			{
				// Otherwise, perform a search
				for (int i = ret.Length - 1; i >= 0; --i)
					ret[i] = PirateSearch.GetPath_Search(SearchQueryWithSeasonAndEpisode, Categories[i]);
			}
			
			return ret;
		}



		public TorrentFilter_Video GetSearchFilter()
		{
			return new TorrentFilter_Video()
			{
				title = SearchQuery,
				resolutionMin = ResMin,
				resolutionMax = ResMax,
				sizeMin = FileSizeMin,
				sizeMax = FileSizeMax,
				releaseTypes = ReleaseTypes,
				videoEncoding = VideoEncoding,
				audioEncoding = AudioEncoding,
				ageMin = AgeMin,
				ageMax = AgeMax,
				minSeedLeech = SeedLeechMin,
				uploaderInclude = UploaderInclude,
				uploaderExclude = UploaderExclude,
				hasComments = HasComments,
				year = Year,
				season = Season,
				episode = Episode,
				notHc = NotHC,
				vip = VIP,
				trusted = Trusted,
				videoType = VideoType
			};
		}



		private VideoType VideoType
		{
			get { return rdbMovie.Checked ? VideoType.Movie : VideoType.TV; }
			set { rdbTv.Checked = !(rdbMovie.Checked = value == VideoType.Movie); }
		}





		// --------------------------- EVENT HANDLERS ---------------------------
		
		private void Close_Search()
		{
			Search?.Invoke(this);
			Hide();
		}



		private void Close_Cancel()
		{
			Hide();
		}



		private void RbMovie_CheckedChanged(object sender, EventArgs e)
		{
			pnlYear.Enabled = ((RadioButton)sender).Checked;
		}

		

		private void RbTv_CheckedChanged(object sender, EventArgs e)
		{
			pnlSeasonEpisode.Enabled = ((RadioButton)sender).Checked;
		}



		private void SearchButton_Click(object sender, EventArgs e)
		{
			Close_Search();
		}



		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);
			
			switch (e.KeyCode)
			{
				case Keys.Escape:
					Close_Cancel();
					break;
			}
		}



		new public void Hide()
		{
			base.Hide();
			Hidden?.Invoke(this);
		}




		// --------------------------- VALUES ---------------------------


		public string SearchQuery
		{
			get { return tbxSearchQuery.Text; }
		}



		public string SearchQueryWithSeasonAndEpisode
		{
			get
			{
				string query = SearchQuery;

				switch (VideoType)
				{
					case VideoType.Movie:
						if (Year > 0)
							query += " " + Year;
						break;

					case VideoType.TV:
						if (Season != 0)
							query += Episode == 0
								? string.Format(" Season {0}", Season)
								: string.Format(" S{0:00}E{1:00}", Season, Episode);
						break;
				}

				return query;
			}
		}


		public int Year
		{
			get
			{
				return VideoType == VideoType.Movie && !string.IsNullOrEmpty(tbxYear.Text)
					? int.Parse(tbxYear.Text)
					: 0;
			}
		}


		public int Season
		{
			get
			{
				return VideoType == VideoType.TV && !string.IsNullOrEmpty(tbxSeason.Text)
					? int.Parse(tbxSeason.Text)
					: 0;
			}
		}


		public int Episode
		{
			get
			{
				return VideoType == VideoType.TV && !string.IsNullOrEmpty(tbxEpisode.Text) && pnlEpisode.Enabled
					? int.Parse(tbxEpisode.Text)
					: 0;
			}
		}


		public int[] Categories
		{
			get { return new int[] { (int)PirateCategory.Category.Video }; }
		}


		public int ResMin
		{
			get { return rngResolution.ValueMin; }
		}


		public int ResMax
		{
			get { return rngResolution.ValueMax; }
		}


		public ulong FileSizeMin
		{
			get { return rngFileSize.ValueMin; }
		}


		public ulong FileSizeMax
		{
			get { return rngFileSize.ValueMax; }
		}


		public TimeSpan AgeMin
		{
			get { return rngAge.ValueMin; }
		}


		public TimeSpan AgeMax
		{
			get { return rngAge.ValueMax; }
		}


		public int SeedLeechMin
		{
			get { return ((ComboBoxItem<int>)cbxSL.SelectedItem).Value; }
		}


		public string[] UploaderInclude
		{
			get { return string.IsNullOrEmpty(tbxUploaderInclude.Text) ? new string[0] : tbxUploaderInclude.Text.ToArray(); }
		}


		public string[] UploaderExclude
		{
			get { return string.IsNullOrEmpty(tbxUploaderExclude.Text) ? new string[0] : tbxUploaderExclude.Text.ToArray(); }
		}


		public bool HasComments
		{
			get { return chbHasComments.Checked; }
		}


		public ReleaseType[] ReleaseTypes
		{
			get { return chlReleaseTypes.GetCheckedItemTags<ReleaseType>(); }
		}


		public VideoEncoding[] VideoEncoding
		{
			get { return chlVideoEncoding.GetCheckedItemTags<VideoEncoding>(); }
		}


		public AudioEncoding[] AudioEncoding
		{
			get { return chlAudioEncoding.GetCheckedItemTags<AudioEncoding>(); }
		}


		public bool NotHC
		{
			get { return chbNotHc.Checked; }
		}


		public bool VIP
		{
			get { return chbVip.Checked; }
		}


		public bool Trusted
		{
			get { return chbTrusted.Checked; }
		}

	}

}
