﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;


namespace TorrentBrowser
{
	public class DropDownCheckList : ComboBox
	{

		private const string LABEL_NONE = "None";
		private const string LABEL_ALL = "All";

		private ListView listView;
		private PopupWindow popup;
		private static readonly StringBuilder sb = new StringBuilder();
		private readonly ListViewItem itemAll;
		private readonly ListViewItem itemNone;
		private int disableItemCheckedCallback = 0;



		public DropDownCheckList()
		{
			// Combo box
			DropDownStyle = ComboBoxStyle.DropDownList;
			DropDownHeight = 1;
			DropDownWidth = 1;
			Items.Add("");
			Label = LABEL_NONE;
			SelectedIndex = 0;
			Width = Width;
			Anchor = AnchorStyles.Left | AnchorStyles.Right;

			// List view
			listView = new ListView();
			listView.Height = 4;
			Add(itemAll =	new ListViewItem(LABEL_ALL));
			Add(itemNone =	new ListViewItem(LABEL_NONE));
			listView.View = View.Details;
			listView.CheckBoxes = true;
			listView.FullRowSelect = true;
			listView.HeaderStyle = ColumnHeaderStyle.None;
			listView.Columns.Add("");
			listView.Anchor = AnchorStyles.Left | AnchorStyles.Right;
			
			Height = 0;
			OnLocationChanged(null);
			OnSizeChanged(null);
		}



		private void ListView_ItemCheck(object sender, ItemCheckedEventArgs e)
		{
			if (disableItemCheckedCallback > 0) {
				return;
			}

			++disableItemCheckedCallback;
			
			// "All" item clicked
			if (e.Item == itemAll)
			{
				if (itemAll.Checked)
				{
					CheckAll();
				}
				else if (NumCheckedItems == listView.Items.Count - 2)
				{
					itemAll.Checked = true;
				}
			}
			// "None" item clicked
			if (e.Item == itemNone)
			{
				if (itemNone.Checked)
				{
					CheckNone();
				}
				else if (NumCheckedItems == 0)
				{
					itemNone.Checked = true;
				}
			}
			// Normal item clicked
			else
			{
				UpdateSpecialItems();
			}

			--disableItemCheckedCallback;
		}



		private void UpdateLabel()
		{
			if (NumCheckedItems == 0)
			{
				// All
				Label = LABEL_NONE;
			}
			else if (NumCheckedItems == listView.Items.Count - 2)
			{
				// None
				Label = LABEL_ALL;
			}
			else
			{
				// Mixed
				sb.Length = 0;

				for (int i = 0; i < listView.CheckedItems.Count; ++i)
				{
					if (listView.CheckedItems[i].Index < 2) {
						continue;
					}

					sb.Append(listView.CheckedItems[i].Text);

					if (i < listView.CheckedItems.Count - 1) {
						sb.Append(", ");
					}
				}

				Label = sb.ToString();
			}
		}



		protected override void OnMouseClick(MouseEventArgs e)
		{
			DroppedDown = false;
			popup = new PopupWindow(listView);
			popup.Show(PointToScreen(new Point(0, 20)));
			listView.ItemChecked += ListView_ItemCheck;
			popup.Closing += (s, e2) => listView.ItemChecked -= ListView_ItemCheck;
		}



		protected override void OnSizeChanged(EventArgs e)
		{
			listView.Width = Width;
			listView.Columns[0].Width = listView.ClientSize.Width;
		}



		private void Parent_MouseClick(object sender, MouseEventArgs e)
		{
			listView.Visible = false;
		}



		public void Add(ListViewItem item)
		{
			listView.Items.Add(item);
			listView.Height += listView.GetItemRect(item.Index).Height;
		}



		public void Remove(ListViewItem item)
		{
			listView.Height -= listView.GetItemRect(item.Index).Height;
			listView.Items.Remove(item);
		}



		public void AddEnum(Type type)
		{
			string[] names = Enum.GetNames(type);
			Array values =  Enum.GetValues(type);

			for (int i = 0; i < names.Length; ++i)
			{
				Add(new ListViewItem(names[i]) { Tag = values.GetValue(i) });
			}
		}



		public T[] GetCheckedItemTags<T>()
		{
			List<T> ret = new List<T>();

			foreach (ListViewItem item in listView.Items)
			{
				if (item.Checked && item.Tag is T)
				{
					ret.Add((T)item.Tag);
				}
			}

			return ret.ToArray();
		}



		public void SetCheckedItems<T>(T[] items)
		{
			for (int i = 0; i < listView.Items.Count; ++i)
			{
				listView.Items[i].Checked = Array.IndexOf(items, listView.Items[i].Tag) != -1;
			}

			UpdateLabel();
		}



		public void CheckAll()
		{
			for (int i = 2; i < listView.Items.Count; ++i)
			{
				listView.Items[i].Checked = true;
			}

			UpdateLabel();
			UpdateSpecialItems();
		}



		public void CheckNone()
		{
			for (int i = 2; i < listView.Items.Count; ++i)
			{
				listView.Items[i].Checked = false;
			}

			UpdateLabel();
			UpdateSpecialItems();
		}



		private void UpdateSpecialItems()
		{
			itemAll.Checked = NumCheckedItems == listView.Items.Count - 2;
			itemNone.Checked = NumCheckedItems == 0;

			UpdateLabel();
		}





		// ----------------------- PROPERTIES -----------------------

		public int Length
		{
			get { return listView.Items.Count; }
		}



		private string Label
		{
			set { Items[0] = value; }
			get { return (string)Items[0]; }
		}



		public int NumCheckedItems
		{
			get
			{
				int numCheckedItems = 0;

				for (int i = 2; i < listView.Items.Count; ++i)
				{
					if (listView.Items[i].Checked)
					{
						++numCheckedItems;
					}
				}

				return numCheckedItems;
			}
		}



		public int[] CheckedItemIndices
		{
			get
			{
				int[] ret = new int[listView.CheckedItems.Count];

				for (int i = 0; i < listView.CheckedItems.Count; ++i)
				{
					ret[i] = listView.CheckedItems[i].Index;
				}

				return ret;
			}

			set
			{
				for (int i = 0; i < listView.Items.Count; ++i)
				{
					listView.Items[i].Checked = false;
				}

				foreach (int index in value)
				{
					listView.Items[index].Checked = true;
				}

				UpdateLabel();
			}
		}

	}
}
