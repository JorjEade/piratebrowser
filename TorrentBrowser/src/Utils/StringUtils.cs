﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;



public enum Language
{
	Unknown,
	Mul,
	Eng,
	Fre,
	Kor,
	Swe,
	Dut,
	Nor,
	Ita,
	Ara,
	Cze,
	Fin,
	Spa,
	Rum,
	Rus,
	Dan,
	Jpn,
	Gre,
	Hin,
	Slo,
	Bul,
	Heb,
}



public static class StringUtils
{

	public static readonly string[] romanNumerals = new string[] { "0", "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix", "x", "xi", "xii", "xiii", "xiv", "xv", "xvi", "xvii", "xviii", "xix" };
	


	static StringUtils()
	{
		// Create language lookup
		Dictionary<string, Language> lookup = new Dictionary<string, Language>();

		foreach (var kvp in languages) {
			lookup.Add(kvp.Key.ToString().ToLower(), kvp.Key);
			foreach (string str in kvp.Value)
				lookup.Add(str, kvp.Key);
		}
		
		languageLookup = new ReadOnlyDictionary<string, Language>(lookup);
	}

	
	public static readonly ReadOnlyDictionary<string, Language> languageLookup;
	

	public static readonly Dictionary<Language, string[]> languages = new Dictionary<Language, string[]>
	{
		{ Language.Mul, new string[] { "m", "multi" } },
		{ Language.Eng, new string[] { "en", "uk", "gb", "english" } },
		{ Language.Fre, new string[] { "fr", "fra", "french" } },
		{ Language.Swe, new string[] { "sv", "swedish" } },
		{ Language.Dut, new string[] { "nl", "ned", "nld", "netherlands" } },
		{ Language.Kor, new string[] { "ko", "korean" } },
		{ Language.Nor, new string[] { "no", "norwegian" } },
		{ Language.Ita, new string[] { "it", "italian" } },
		{ Language.Ara, new string[] { "ar", "arabic" } },
		{ Language.Cze, new string[] { "cs", "ces", "czech" } },
		{ Language.Fin, new string[] { "fi", "finnish" } },
		{ Language.Spa, new string[] { "es", "esp", "spanish" } },
		{ Language.Rum, new string[] { "ro", "rom", "ron", "romanian" } },
		{ Language.Rus, new string[] { "ru", "russian" } },
		{ Language.Dan, new string[] { "da", "dk", "danish" } },
		{ Language.Jpn, new string[] { "ja", "jap", "japanese" } },
		{ Language.Gre, new string[] { "el", "ell", "greek" } },
		{ Language.Hin, new string[] { "hi", "hindi", "tamil" } },
		{ Language.Slo, new string[] { "sk", "slk", "slovak" } },
		{ Language.Bul, new string[] { "bg", "bulgarian" } },
		{ Language.Heb, new string[] { "he", "hebrew" } },
	};



	public static Language GetLanguage(string input)
	{
		Language ret = Language.Unknown;
		languageLookup.TryGetValue(input.ToLower(), out ret);
		return ret;
	}



	public static string ReplaceRomanNumeralsWithDigits(string input, int digitMin = 1, int digitMax = int.MaxValue)
	{
		if (digitMax == int.MaxValue) {
			digitMax = romanNumerals.Length - 1;
		}

		if (digitMax >= romanNumerals.Length) {
			throw new IndexOutOfRangeException("Numerals only go up to " + (romanNumerals.Length - 1).ToString());
		}

		for (int i = digitMin; i <= digitMax; ++i) {
			input = ReplaceRomanNumeral(input, i);
		}

		return input;
	}



	public static string ReplaceRomanNumeral(string input, int number)
	{
		return Regex.Replace(
			input,
			@"(?<=\s)" + romanNumerals[number] + @"(?=\s|$)",
			number.ToString(),
			RegexOptions.IgnoreCase
		);
	}



	public static string StripTags(string input)
	{
		// Remove HTML tags and formatting
		input = WebUtility.HtmlDecode(Regex.Replace(input, "<[^>]*(>|$)", string.Empty));

		input = NormaliseWhite(input);

		return input;
	}



	public static string NormaliseWhite(string input)
	{
		return Regex.Replace(input, @"[\s\r\n]+", " ").Trim();
	}



	public static string ReplaceRecursive(string input, string replacement, params string[] patterns)
	{
		foreach (string pattern in patterns)
		{
			if (Regex.IsMatch(input, pattern))
			{
				return ReplaceRecursive(
					Regex.Replace(input, pattern, replacement),
					replacement,
					patterns
				);
			}
		}

		return input;
	}



	public static string FileSizeString(ulong bytes)
	{
		float bytesf = bytes;
		string[] sizes = { "B", "KB", "MB", "GB", "TB" };
		int order = 0;

		while (bytesf >= 1024 && order < sizes.Length - 1) {
			order++;
			bytesf /= 1024;
		}

		return string.Format("{0:0.#} {1}", bytesf, sizes[order]);
	}



	public static string Age(this TimeSpan timeSpan, bool beSpecificIfUnder24Hours = true)
	{
		return AgeOrAgo(timeSpan, false, beSpecificIfUnder24Hours);
	}



	public static string Age(this DateTime date, bool beSpecificIfUnder24Hours = true)
	{
		return AgeOrAgo(DateTime.Now - date, false, beSpecificIfUnder24Hours);
	}



	public static string Ago(this TimeSpan timeSpan, bool beSpecificIfUnder24Hours = true)
	{
		return AgeOrAgo(timeSpan, true, beSpecificIfUnder24Hours);
	}



	public static string Ago(this DateTime date, bool beSpecificIfUnder24Hours = true)
	{
		return AgeOrAgo(DateTime.Now - date, true, beSpecificIfUnder24Hours);
	}



	private static string AgeOrAgo(TimeSpan timeSpan, bool includeAgo, bool beSpecificIfUnder24Hours)
	{
		string agoSuffix = includeAgo ? " ago" : "";

		if (beSpecificIfUnder24Hours)
		{
			if (timeSpan.TotalSeconds < 60)	return Pluralise(timeSpan.Seconds, "sec") + agoSuffix;
			if (timeSpan.TotalMinutes < 60)	return Pluralise(timeSpan.Minutes, "min") + agoSuffix;
			if (timeSpan.TotalHours < 24)	return Pluralise(timeSpan.Hours, "hour") + agoSuffix;
		}

		TimeSpan timeIntoToday = DateTime.Now - new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
		int days = (timeSpan - timeIntoToday + TimeSpan.FromDays(1)).Days;

		return
			days == 0 ?		"Today" :
			days == 1 ?		"Yesterday" :
			days < 7 ?		days + " days" + agoSuffix :
			days < 30 ?		Pluralise((int)Math.Round(days / 7f), "week") + agoSuffix :
			days < 365 ?	Pluralise((int)Math.Round(days / 30f), "month") + agoSuffix :
							Pluralise((int)Math.Round(days / 365f), "year") + agoSuffix;
	}



	public static string Pluralise(double num, string unitName)
	{
		return
			string.Format("{0} {1}{2}",
				num,
				unitName,
				num != 1 ? "s" : string.Empty
			);
	}



	public static string ResolutionString(int resolution)
	{
		switch (resolution)
		{
			case 0:		return "";
			case 2160:	return "4K";
			case 4320:	return "8K";
			default:	return resolution + "p";
		}
	}


	public static bool Compare(string a, string b)
	{
		if (!string.IsNullOrEmpty(a) && !string.IsNullOrEmpty(b))
			return a == b;

		if (string.IsNullOrEmpty(a) && string.IsNullOrEmpty(b)) {
			Console.WriteLine("Can't compare - both a and b are null or empty");
		}
		else if (string.IsNullOrEmpty(a)) {
			Console.WriteLine("Can't compare - a is null or empty");
		}
		else if (string.IsNullOrEmpty(b)) {
			Console.WriteLine("Can't compare - b is null or empty");
		}

		return false;
	}

}
