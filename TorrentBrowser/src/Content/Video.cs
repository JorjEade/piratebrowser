﻿using System;
using System.Collections.Generic;


namespace TorrentBrowser
{

	public enum Genre
	{
		Action =			28,	
		Adventure =			12,	
		Animation =			16,	
		Comedy =			35,	
		Crime =				80,	
		Documentary =		99,	
		Drama =				18,	
		Family =			10751,
		Fantasy =			14,	
		History =			36,	
		Horror =			27,
		Kids =				10762,
		Music =				10402,
		Mystery =			9648,
		Romance =			10749,
		SciFi =				878,
		SciFiAndFantasy =	10765,
		TVMovie =			10770,
		Thriller =			53,
		War =				10752,
		Western =			37
	}

	public class Video : Content
	{
		public const string NO_POSTER_URL = "";
		public const string NO_BACKDROP_URL = "";

		public static readonly DateTime NoReleaseDate = DateTime.MinValue;

		public readonly string titleAndYear;
		public readonly DateTime releaseDate;
		public readonly string description;
		public readonly float normScore;
		public readonly Genre[] genres;
		public readonly bool hasPosterUrl;
		public readonly bool hasBackdropUrl;

		private readonly string posterUrlFormat;
		private readonly string backdropUrlFormat;
		private readonly string titleSimple;
		private readonly string titleSimpleAndYear;




		public Video(string id, string title = null, DateTime? releaseDate = null, string description = null, float normScore = -1, Genre[] genres = null, string posterUrlFormat = "", string backdropUrlFormat = "", params Torrent_Video[] torrents) : base(id, title, torrents)
		{
			this.titleSimple = title.ToLower().TrimStart("the ");
			this.releaseDate = releaseDate.HasValue ? releaseDate.Value : NoReleaseDate;
			this.titleAndYear = title + (this.releaseDate != NoReleaseDate ? " (" + this.releaseDate.Year.ToString() + ")" : "");
			this.titleSimpleAndYear = titleSimple + (this.releaseDate != NoReleaseDate ? " (" + this.releaseDate.Year.ToString() + ")" : "");
			this.normScore = normScore;
			this.description = description;
			this.genres = genres;
			this.posterUrlFormat = posterUrlFormat;
			this.backdropUrlFormat = backdropUrlFormat;

			hasPosterUrl = !string.IsNullOrEmpty(posterUrlFormat);
			hasBackdropUrl = !string.IsNullOrEmpty(backdropUrlFormat);
		}



		public string GetPosterUrl(string size)
		{
			return hasPosterUrl ? string.Format(posterUrlFormat, size) : NO_POSTER_URL;
		}



		public string GetBackdropUrl(string size)
		{
			return hasBackdropUrl ? string.Format(backdropUrlFormat, size) : NO_BACKDROP_URL;
		}



		public List<Torrent_Video> TorrentsVideos
		{
			get { return Torrents.ConvertAll(x => (Torrent_Video)x); }
		}



		public override bool IsSameAs(Content other)
		{
			Video otherVideo = other as Video;

			if (otherVideo == null)
				return false;

			if (releaseDate != NoReleaseDate && otherVideo.releaseDate != NoReleaseDate)
				return StringUtils.Compare(titleSimpleAndYear, otherVideo.titleSimpleAndYear);

			return StringUtils.Compare(titleSimple, otherVideo.titleSimple);
		}

	}
}
