﻿using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Threading;


namespace TorrentBrowser
{
	public class TMDbSearch : IDisposable
	{
		
		public enum EState
		{
			NotStarted,
			Loading,
			Complete,
			Error,
			Disposed
		}

		public EState State { get { lock (getStateLock) return state; } private set { lock (getStateLock) state = value; } } private EState state = EState.NotStarted; private readonly object getStateLock = new object();
		public Exception Exception { get { lock (getExeptionLock) return exception; } private set { lock (getExeptionLock) exception = value; } } private Exception exception; private readonly object getExeptionLock = new object();
		public TMDbSearchResponse.Result Result { get; private set; }
		public readonly string Title;
		public readonly int Year;
		public readonly ContentType ContentType;

		private CancellationTokenSource cancellationToken;
			


		public TMDbSearch(string title, int year, ContentType contentType = ContentType.movie)
		{
			Title = title;
			Year = year;
			ContentType = contentType;
		}



		public void DoSearch(CancellationTokenSource cancellationToken = null)
		{
			this.cancellationToken = cancellationToken;

			State = EState.Loading;

			int year = Year;

			retry:
				
			// Limit requests
			lock (throttle)
				throttle.EventAboutToOccur();

			// Check if cancelled
			if (cancellationToken != null && cancellationToken.IsCancellationRequested)
				return;

			string responseJson = null;

			try
			{
				responseJson = Utils.LoadData(
					string.Format(SEARCH_URL, Title, year, ContentType),
					Encoding.UTF8
				);
			}
			catch (WebException e)
			{
				Console.WriteLine("TMDb search HTTP error response: {0}", ((HttpWebResponse)e.Response).StatusCode);
					
				switch (((HttpWebResponse)e.Response).StatusCode)
				{
					case (HttpStatusCode)429: // "Too Many Requests"
						if (cancellationToken != null && cancellationToken.IsCancellationRequested)
							return;
						goto retry;
				}
			}
			catch (Exception e)
			{
				ThrowException(e);
			}

			// Check if cancelled
			if (cancellationToken != null && cancellationToken.IsCancellationRequested)
				return;

			// Check if response was empty
			if (string.IsNullOrEmpty(responseJson))
				ThrowException(new Exception("TMDb search: JSON response was empty"));

			// Parse result
			TMDbSearchResponse responseObject = TMDbSearchResponse.FromJson(responseJson);

			// Check for 0 results
			if (responseObject.total_results == 0)
			{
				Result = null;

				// If searched with year, try again without
				if (year != 0) {
					year = 0;
					goto retry;
				}

				Console.WriteLine("No results returned for TMDb search: title = {0}; year = {1}; type = {2}", Title, year, ContentType);
			}
			else
			{
				// Find the result with the correct year
				Result =
					Array.Find(responseObject.results, r => r.Date.Year == year)
					?? responseObject.results[0];
			}

			State = EState.Complete;
		}



		private void ThrowException(Exception e)
		{
			Exception = e;
			State = EState.Error;
			throw Exception;
		}



		public bool IsSameSearch(string title, int year, ContentType contentType)
		{
			return
				title == Title &&
				year == Year &&
				contentType == ContentType;
		}



		public void Dispose()
		{
			State = EState.Disposed;
			cancellationToken?.Cancel();
		}





		// -------------------------------------- STATIC HELPERS --------------------------------------

		private const string SEARCH_URL = "https://api.themoviedb.org/3/search/{2}?api_key=" + TMDbDefines.API_KEY + "&query={0}&year={1}";
		private const double BUCKET_DURATION = 10;
		private const int BUCKET_REQUEST_LIMIT = 40;

		private static readonly List<TMDbSearch> allSearches = new List<TMDbSearch>();

		private static readonly EventThrottle throttle = new EventThrottle(
			BUCKET_DURATION,
			BUCKET_REQUEST_LIMIT,
			t => Console.WriteLine("Throttle start: TMDb..."),
			t => Console.WriteLine("...Throttle end: TMDb")
		);



		public static TMDbSearchResponse.Result DoSearch(string title, int year, ContentType contentType = ContentType.movie, CancellationTokenSource cancellationToken = null)
		{
			TMDbSearch search = GetSearch(title, year, contentType, cancellationToken);

			switch (search.State)
			{
				case EState.Complete:
					// If search is already completed, return its result
					return search.Result;

				case EState.NotStarted:
				case EState.Loading:
					// If not started, or loading, wait for it to complete
					while (search.State == EState.NotStarted || search.State == EState.Loading) { }

					switch (search.State)
					{
						case EState.Complete:
							return search.Result;

						case EState.Error:
							throw search.Exception;

						default:
							 return null;
					}
					
				default:
					return null;
			}
		}



		private static TMDbSearch GetSearch(string title, int year, ContentType contentType = ContentType.movie, CancellationTokenSource cancellationToken = null)
		{
			TMDbSearch search;

			lock (allSearches)
			{
				// Find existing search
				search = allSearches.Find(s => s.IsSameSearch(title, year, contentType) && s.State != EState.Disposed);

				if (search != null) {
					//Console.WriteLine("FOUND EXISTING SEARCH: " + search.State + ": " + search.Title + " (" + search.Year + ")");
					return search;
				}

				// Create a new search
				search = new TMDbSearch(title, year, contentType);
				
				allSearches.Add(search);
				//Console.WriteLine("STARTED NEW SEARCH: " + title + " (" + year + ")");
			}

			// Do search
			search.DoSearch(cancellationToken);

			switch (search.State)
			{
				case EState.Disposed:
				case EState.Error:
					lock (allSearches)
						allSearches.Remove(search);
					break;
			}

			//Console.WriteLine("...NEW SEARCH COMPLETE: " + title + " (" + year + ")");
			return search;
		}




		public static void DisposeAll()
		{
			lock (allSearches)
				allSearches.DisposeAndClear();
		}
	}

}
