﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace TorrentBrowser
{

	public class TMDbSearchResponse
	{
		public int page;
		public int total_results;
		public int total_pages;
		public Result[] results;



		public static TMDbSearchResponse FromJson(string json)
		{
			return JsonConvert.DeserializeObject<TMDbSearchResponse>(json);
		}



		public class Result
		{
			public int vote_count;
			public int id;
			public bool video;
			public float vote_average;
			public string title;
			public string name;
			public float popularity;
			public string poster_path;
			public string original_language;
			public string original_title;
			public string original_name;
			public int[] genre_ids;
			public string backdrop_path;
			public bool adult;
			public string overview;
			public string release_date;
			public string first_air_date;



			public DateTime Date
			{
				get
				{
					return
						!string.IsNullOrEmpty(release_date)
							? DateTime.Parse(release_date)
						: !string.IsNullOrEmpty(first_air_date)
							? DateTime.Parse(first_air_date)
							: Video.NoReleaseDate;
				}
			}



			public Genre[] GetGenreArray()
			{
				List<Genre> ret = new List<Genre>();

				for (int i = 0; i < genre_ids.Length; ++i)
				{
					ret.Add((Genre)genre_ids[i]);
				}

				return ret.ToArray();
			}

		}
	}
}
