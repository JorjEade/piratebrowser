﻿using System.Linq;


namespace TorrentBrowser
{
	public class TorrentFilter
	{

		public int[] allowedCategories;
		public int[] allowedSubcategories;



		public virtual bool MeetsCriteria(Torrent torrent)
		{
			return
				torrent != null &&
				(allowedCategories == null ||		allowedCategories.Contains(torrent.category)) &&
				(allowedSubcategories == null ||	allowedSubcategories.Contains(torrent.subcategory));
		}

	}
}
