﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;


namespace TorrentBrowser
{
	public enum VideoEncoding
	{
		Unknown,
		DivX,
		Xvid,
		AVC,
		HEVC,
		NTSC,
		WMV,
		MP4,
		OGG,
		AVI
	}


	public enum AudioEncoding
	{
		Unknown,
		MP3,
		AAC,
		AC3,
		DTS,
		FLAC
	}


	public enum VideoType
	{
		Movie,
		TV
	}


	public class Torrent_Video : Torrent
	{
		private List<string> regexGroups = new List<string>();

		public readonly string title;
		public readonly string titleAndYear;
		public readonly DateTime releaseDate = Video.NoReleaseDate;
		public readonly int resolution;
		public readonly ReleaseType releaseType;
		public readonly string releaseSubType;
		public readonly string releaseTypeAndSubType;
		public readonly VideoEncoding videoEncoding = VideoEncoding.Unknown;
		public readonly AudioEncoding audioEncoding = AudioEncoding.Unknown;
		public readonly Language language;
		public readonly Language subsLanguage;
		public readonly bool embeddedSubs;
		public readonly Language dubLanguage;
		public readonly int season;
		public readonly int seasonMax;
		public readonly int episode;
		public readonly int episodeMax;
		public readonly string seasonEpisodeString;
		public readonly VideoType videoType;
		public readonly bool isHc;
		public readonly int seriesOrder;



		public Torrent_Video(VideoType videoType, int category, int subcategory, string pageUrl, string rawTitle, string magnetLink, int numComments, bool isTrusted, bool isVip, DateTime uploadDate, ulong size, string uploadedBy, int numSeeders, int numLeechers) : base(category, subcategory, pageUrl, rawTitle, magnetLink, numComments, isTrusted, isVip, uploadDate, size, uploadedBy, numSeeders, numLeechers)
		{
			this.videoType = videoType;
			

			// ------------ EXTRACT DETAILS FROM TITLE ------------

			int titleLength = cleanTitle.Length;
			///<summary>word boundary</summary>
			const string WB = @"(?:\(|\)| |^|$)";
			const string WB_WITH_HYPHEN = @"(?:\(|\)| |^|-|$)";
			const string NUMBER_RANGE = @"(?:(?<!\d)(\d{1,2})(?: ?(?:-|and) ?(\d{1,2}))?(?!\d))";
			const string SUB_SUFFIX = @"(?: ?([emw])? ?sub(?:title)?s?)";
			const string DUB_SUFFIX = @"(?: ?dub(?:s|bed)?)";
			const string SEASON = @"(?:season|series|stagione)";
			const string EPISODE = @"(?:e(?:p(?:isode)?)?)";
			

			// SxE
			if (ParseCleanTitle(ref titleLength, WB_WITH_HYPHEN + NUMBER_RANGE + @"x" + NUMBER_RANGE)) {
				int.TryParse(regexGroups[0], out season);
				int.TryParse(regexGroups[1], out seasonMax);
				int.TryParse(regexGroups[2], out episode);
				int.TryParse(regexGroups[3], out episodeMax);
			}
			// Season S E
			else if (ParseCleanTitle(ref titleLength, WB_WITH_HYPHEN + SEASON + @" (\d{1,2}) " + NUMBER_RANGE)) {
				int.TryParse(regexGroups[0], out season);
				int.TryParse(regexGroups[1], out episode);
				int.TryParse(regexGroups[2], out episodeMax);
			}
			else
			{
				// Season(s) S (E?)
				if (ParseCleanTitle(ref titleLength, WB_WITH_HYPHEN + @"(?:" + SEASON + @"s?|t|s) ?" + NUMBER_RANGE + NUMBER_RANGE + "?")) {
					int.TryParse(regexGroups[0], out season);
					int.TryParse(regexGroups[1], out seasonMax);
					int.TryParse(regexGroups[2], out episode);
					int.TryParse(regexGroups[3], out episodeMax);
				}

				// Episode(s) E
				if (ParseCleanTitle(ref titleLength, @"[ \d(-]" + EPISODE + @"s? ?" + NUMBER_RANGE)) {
					int.TryParse(regexGroups[0], out episode);
					int.TryParse(regexGroups[1], out episodeMax);
				}
			}
			// S-E
			if (season == 0 && episode == 0)
			{
				if (ParseCleanTitle(ref titleLength, WB_WITH_HYPHEN + SEASON + @"? ?(\d{1,2})-(\d{1,2})" + WB)) {
					int.TryParse(regexGroups[0], out season);
					int.TryParse(regexGroups[1], out episode);
				}
			}


			// If found a season or episode, assume this is a TV show
			if (season != 0 || episode != 0)
			{
				this.videoType = VideoType.TV;

				// If have found episode but not season, assume season is 1
				if (season == 0 && episode > 0) {
					season = 1;
				}

				// Ensure season/episode max is at least as much as season/episode
				seasonMax = Math.Max(season, seasonMax);
				episodeMax = Math.Max(episode, episodeMax);

				// Set series order
				seriesOrder = season * 1000 + episode;
			}


			// No longer need hyphens, so replace with a space
			cleanTitle = cleanTitle.Replace('-', ' ');


			// Year
			if (ParseCleanTitle(ref titleLength, @"[([]?(19\d\d|20[0-2]\d)[)\]]?(?!p)"))
				releaseDate = new DateTime(int.Parse(regexGroups[0]), 1, 1);

			
			// Subs language
			bool setSubLangToMainLang = false;
			if (ParseCleanTitle(ref titleLength, WB + @"(\w+)" + SUB_SUFFIX + WB))
			{
				embeddedSubs =	regexGroups[1].ToLower() == "e";

				if (regexGroups[1].ToLower() == "m") {
					subsLanguage = Language.Mul;
				}
				else if ((subsLanguage = StringUtils.GetLanguage(regexGroups[0])) == Language.Unknown) {
					Console.WriteLine("UNKNOWN SUBS \"" + regexGroups[0] + "\": " + cleanTitle);
					setSubLangToMainLang = true;
				}
			}


			// Dubs language
			bool setDubLangToMainLang = false;
			if (ParseCleanTitle(ref titleLength, WB + @"([a-z]+)?" + DUB_SUFFIX + WB)) {
				if (string.IsNullOrEmpty(regexGroups[0]))
					setDubLangToMainLang = true;
				else {
					dubLanguage = StringUtils.GetLanguage(regexGroups[0]);
					if (dubLanguage == Language.Unknown) {
						Console.WriteLine("UNKNOWN DUB: " + rawTitle);
						setDubLangToMainLang = true;
					}
				}
			}


			// Resolution
			if (ParseCleanTitle(ref titleLength, WB + @"([o\d]{3,4})p" + WB, RegexOptions.RightToLeft))
				resolution = int.Parse(regexGroups[0].ToLower().Replace('o', '0'));
			if (ParseCleanTitle(ref titleLength, @"(?<!\d)720(?!\d)", RegexOptions.RightToLeft))
				resolution = 720;
			if (ParseCleanTitle(ref titleLength, @"(?<!\d)1080(?!\d)", RegexOptions.RightToLeft))
				resolution = 1080;
			if (ParseCleanTitle(ref titleLength, @"\D4k", RegexOptions.RightToLeft))
				resolution = 2160;
			if (ParseCleanTitle(ref titleLength, @"\D8k", RegexOptions.RightToLeft))
				resolution = 4320;

			
			// Video encoding
			for (int i = 1, count = Enum.GetNames(typeof(VideoEncoding)).Length; i < count; ++i)
				if (ParseCleanTitle(ref titleLength, WB + (VideoEncoding)i + WB, RegexOptions.RightToLeft))
					videoEncoding = (VideoEncoding)i;
			if (ParseCleanTitle(ref titleLength, WB + "[xh] ?264" + WB, RegexOptions.RightToLeft))
				videoEncoding = VideoEncoding.AVC;
			if (ParseCleanTitle(ref titleLength, WB + "[xh] ?265" + WB, RegexOptions.RightToLeft))
				videoEncoding = VideoEncoding.HEVC;
			if (ParseCleanTitle(ref titleLength, WB + "ogm" + WB, RegexOptions.RightToLeft))
				videoEncoding = VideoEncoding.OGG;


			// Audio encoding
			for (int i = 1, count = Enum.GetNames(typeof(AudioEncoding)).Length; i < count; ++i)
				if (ParseCleanTitle(ref titleLength, WB + (AudioEncoding)i + WB, RegexOptions.RightToLeft))
					audioEncoding = (AudioEncoding)i;


			// Release type
			int posOfReleaseType;
			releaseType = ReleaseTypeUtils.GetReleaseType(cleanTitle, out posOfReleaseType, out releaseSubType);
			if (releaseType != ReleaseType.Unknown) {
				titleLength = Math.Min(titleLength, posOfReleaseType);
				releaseTypeAndSubType = releaseType.ToString() + (releaseSubType == releaseType.ToString().ToLower() ? "" : " (" + releaseSubType + ")");
			}


			// HC
			if (ParseCleanTitle(ref titleLength, WB + @"hc" + WB))
				isHc = true;


			// Language
			foreach (var kvp in StringUtils.languageLookup)
				if (Regex.IsMatch(cleanTitle, @"^.{" + titleLength + ",}?" + WB + kvp.Key + WB + @"(?!" + SUB_SUFFIX + "|" + DUB_SUFFIX + ")", RegexOptions.IgnoreCase)) {
					language = kvp.Value;
					break;
				}

			if (setSubLangToMainLang) subsLanguage = language != Language.Unknown ? language : Language.Eng;
			if (setDubLangToMainLang) dubLanguage = language != Language.Unknown ? language : Language.Eng;


			// Title
			title = cleanTitle.Substring(0, titleLength);
			title = title.Trim();
			title = title.ToLower();
			title = Regex.Replace(title, @"\s*\(.*", ""); // Remove anything in brackets
			title = Regex.Replace(title, @" *(hq|hd|imax|3d) *", "");
			title = Regex.Replace(title, @"(?<!^)(the ?)?((full|complete|extended|special|theatrical|directors?|remastered|original|all|new) ?)+(version|movie|release|series|seasons?|cuts?|collections?|release|trilogy|extras|(ed(i(t(i(o(n(s?)?)?)?)?)?)?))?$", "");
			title = StringUtils.NormaliseWhite(title);


			// ID for content
			IdForContent = title.TrimStart("the ") + releaseDate.Year;
		}



		private bool ParseCleanTitle(ref int titleLength, string expression, RegexOptions options = RegexOptions.None)
		{
			Match match = Regex.Match(cleanTitle, expression, options | RegexOptions.IgnoreCase);

			// Bail if no match
			if (!match.Success)
				return false;

			// Reduce titleLength
			titleLength = Math.Min(titleLength, match.Index);

			// Extract matches
			regexGroups.Clear();
			if (match.Groups.Count > 1)
				for (int i = 1; i < match.Groups.Count; ++i)
					regexGroups.Add(match.Groups[i].Value);

			return true;
		}



		public override int CompareTo(Torrent other)
		{
			Torrent_Video v = other as Torrent_Video;

			if (v == null)
				return base.CompareTo(other);

			if (v.videoType == VideoType.TV && videoType == VideoType.TV)
				return Math.Sign(seriesOrder - v.seriesOrder);

			return Math.Sign(v.resolution - resolution);
		}

	}
}
