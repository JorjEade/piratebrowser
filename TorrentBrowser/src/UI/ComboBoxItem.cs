﻿using System;


public class ComboBoxItem<T>
{
	public string Label;
	public T Value;


	public ComboBoxItem(string label, T value)
	{
		Label = label;
		Value = value;
	}


	public override string ToString()
	{
		return Label;
	}
}
