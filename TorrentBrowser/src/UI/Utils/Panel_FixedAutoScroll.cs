﻿using System.Drawing;
using System.Windows.Forms;


class Panel_FixedAutoScroll : Panel
{
	/// <summary> Hack to fix AutoScroll issue </summary>
	protected override Point ScrollToControl(Control activeControl)
	{
		return DisplayRectangle.Location;
	}
}
