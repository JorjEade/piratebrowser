﻿using System;
using System.Windows.Forms;



public static class Animation
{

	public const int FPS_DEFAULT = 70;

	public delegate double TweenFunc(double t);
	

	
	///<returns> A function, which when called, stops and disposes the tween </returns>
	public static Token Tween(Action<double> onUpdate, double from, double to, double duration, double fps = FPS_DEFAULT, TweenFunc tweenFunc = null, Action onComplete = null)
	{
		if (duration == 0) {
			onUpdate(to);
			onComplete?.Invoke();
			return new Token();
		}

		DateTime startTime = DateTime.Now;
		Timer timer = new Timer();
		timer.Interval = (int)(1000 / fps);
		tweenFunc = tweenFunc ?? EaseInOut;

		EventHandler onTick = (sender, e) =>
		{
			double t = tweenFunc((DateTime.Now - startTime).TotalSeconds / duration);
			onUpdate(MathUtils.Lerp(from, to, t));

			if (t == 1) {
				timer.Stop();
				timer.Dispose();
				onComplete?.Invoke();
			}
		};

		timer.Tick += onTick;
		timer.Start();

		return new Token(timer);
	}



	private static double EaseInOut(double t)
	{
		return 0.5 + 0.5 * Math.Sin(-Math.PI * 0.5 + Math.PI * MathUtils.Clamp01(t));
	}



	public class Token
	{
		private Timer timer;

		public Token(Timer timer = null)
		{
			this.timer = timer;
		}

		public void Cancel()
		{
			if (timer == null) {
				return;
			}

			timer.Stop();
			timer.Dispose();
			timer = null;
		}
	}

}
