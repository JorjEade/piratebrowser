﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;
using TorrentBrowser.Torrents;
using TorrentBrowser.PirateBay;
using TorrentBrowser.TMDb;



namespace TorrentBrowser
{

	public class Main : Form
	{

		private const string DEFAULT_PIRATE_BAY_DOMAIN = "https://pirateproxy.live";
		private const int GRID_ROWS = 3;
		private const int GRID_COLS = 6;
		private const int RESULTS_PER_GRID = GRID_ROWS * GRID_COLS;

		private Timer checkScrollTimer;
		private PirateSearch search;
		private Panel posterContainer;
		private Size posterSize = new Size();
		private Animation.Token fadeInAnim;
		private Curtain backgroundDimmer;
		private readonly List<VideoInfoPopup> infoPopups = new List<VideoInfoPopup>();
		private readonly List<Poster> posters = new List<Poster>();
		private readonly SearchForm searchForm = new SearchForm();
		
		
		
		public Main()
		{
			// Initialise form
			SuspendLayout();
			//ComponentResourceManager resources = new ComponentResourceManager(typeof(Main));
			//Icon = (Icon)resources.GetObject("$this.Icon");
			ControlBox = false;
			Name = "TorrentBrowser";
			FormBorderStyle = FormBorderStyle.None;
			WindowState = FormWindowState.Maximized;
			Opacity = 0;
			KeyPreview = true;
			BackColor = Color.Black;
			ResumeLayout(false);
			

			// Create background dimmer
			backgroundDimmer = new Curtain();
			backgroundDimmer.MaxOpacity = 0.95;
			backgroundDimmer.Opacity = 0;
			backgroundDimmer.FadeDuration = 0;//0.5;
			Controls.Add(backgroundDimmer);
			backgroundDimmer.Cursor = Cursors.Hand;
			backgroundDimmer.Click += (a, b) => infoPopups.ForEachReverse(p => p.Close());

			
			// Create poster container
			posterContainer = new Panel_FixedAutoScroll();
			posterContainer.Dock = DockStyle.Fill;
			posterContainer.AutoScroll = true;
			Controls.Add(posterContainer);


			// Create search form
			searchForm.Search += OnSearch;


			// Start checking if poster container scrolled to end
			checkScrollTimer = new Timer();
			checkScrollTimer.Interval = 500;
			checkScrollTimer.Tick += CheckIfScrolledToEnd;
			checkScrollTimer.Start();
		}

		

		protected override void OnLoad(EventArgs e)
		{
			fadeInAnim = Animation.Tween(
				onUpdate:	val => Opacity = val,
				from:		0,
				to:			1,
				duration:	1
			);

			// Set poster size
			posterSize.Width = (posterContainer.ClientSize.Width - SystemInformation.VerticalScrollBarWidth) / GRID_COLS;
			posterSize.Height = (int)(posterSize.Width * Poster.WIDTH_TO_HEIGHT);
			
			// Search
			//ShowSearchForm();
			DefaultSearch();
			/*
			NewSearch(
				PirateSearch.GetPath_Search(
					"lord of the rings",
					(int)PirateCategory.SubcategoryVideo.HDMovies,
					(int)PirateCategory.SubcategoryVideo.Movies
				)
			);
			*/
		}



		private void ClearSearch()
		{
			if (search == null) {
				return;
			}

			search?.Dispose();
			search = null;
			posters.ForEach(p => DestroyPoster(p));
			posters.Clear();
		}



		private void DefaultSearch(params TorrentFilter[] filters)
		{
			NewSearch(
				PirateSearch.GetPath_BrowseCategory((int)PirateCategory.SubcategoryVideo.HDMovies),
				filters
			);
		}



		private void NewSearch(string path, params TorrentFilter[] filters)
		{
			ClearSearch();

			search = new PirateSearch(DEFAULT_PIRATE_BAY_DOMAIN, path);
			search.ResultFilters.AddRange(filters);
			search.EntityCreated += OnEntityCreated;
			search.ThrottleEntityCreated.Set(interval: 4, maxEvents: RESULTS_PER_GRID);
			search.LoadMoreResults(RESULTS_PER_GRID);
		}



		private void OnEntityCreated(Entity entity)
		{
			// Create posters for videos
			Video videoEntity = entity as Video;

			if (videoEntity != null) {
				AddPoster(videoEntity);
			}
		}



		private void AddPoster(Video video)
		{
			Poster poster = new Poster(posterSize.Width, posterSize.Height, video);
			int posterIndex = posters.Count;
			int row = posterIndex / GRID_COLS;
			int col = posterIndex % GRID_COLS;
			posterContainer.Controls.Add(poster);
			poster.Location = new Point(col * posterSize.Width, row * posterSize.Height - posterContainer.VerticalScroll.Value);
			poster.Clicked += OnPosterClicked;
			posters.Add(poster);
		}



		private void DestroyPoster(Poster poster)
		{
			posterContainer.Controls.Remove(poster);
			poster.Clicked -= OnPosterClicked;
			poster.Dispose();
		}



		private void OnPosterClicked(Poster poster)
		{
			VideoInfoPopup popup = new VideoInfoPopup(poster.Video);
			popup.ClientSize = new Size((int)(posterSize.Width * 3.5), posterSize.Height);
			popup.Show(this);
			popup.FormClosed += OnPopupClosed;
			infoPopups.Add(popup);

			if (infoPopups.Count == 1) {
				DimBackground();
			}
		}



		private void OnPopupClosed(object o, FormClosedEventArgs e)
		{
			infoPopups.Remove((VideoInfoPopup)o);

			if (infoPopups.Count == 0) {
				UnDimBackground();
			}
		}



		private void CheckIfScrolledToEnd(object sender, EventArgs e)
		{
			float threshold = posterSize.Height * 2;

			if (search != null &&
				search.State == PirateSearch.EState.Ready &&
				//posterContainer.VerticalScroll.Maximum > posterContainer.ClientSize.Height &&
				posterContainer.DistanceFromMaxScroll() < threshold
			) {
				search.LoadMoreResults(RESULTS_PER_GRID);
			}
		}



		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);
			
			switch (e.KeyCode)
			{
				case Keys.Escape:
					searchForm.Dispose();
					Application.Exit();
					break;

				case Keys.Down:
					Extensions.Scroll(posterContainer, 20);
					break;

				case Keys.Up:
					Extensions.Scroll(posterContainer, -20);
					break;
					
				case Keys.PageDown:
					posterContainer.PageDown();
					break;

				case Keys.PageUp:
					posterContainer.PageUp();
					break;
					
				case Keys.Home:
					posterContainer.ScrollHome();
					break;
					
				case Keys.End:
					posterContainer.ScrollEnd();
					break;

				case Keys.C:
					search?.Dispose();
					break;

				case Keys.F:
					ShowSearchForm();
					break;
			}
		}



		public void DimBackground()
		{
			backgroundDimmer.FadeIn();
		}



		public void UnDimBackground()
		{
			backgroundDimmer.FadeOut();
		}



		private void ShowSearchForm()
		{
			searchForm.Show(this);
		}



		private void OnSearch(SearchForm form)
		{
			if (string.IsNullOrEmpty(form.Title))
			{
				DefaultSearch(
					form.GetSearchFilter()
				);
			}
			else
			{
				NewSearch(
					form.GetSearchUrl(),
					form.GetSearchFilter()
				);
			}
		}



		protected override void Dispose( bool disposing )
		{
			search?.Dispose();
			fadeInAnim?.Cancel();
			checkScrollTimer?.Stop();
			checkScrollTimer?.Dispose();
			TMDbSearch.Dispose();
		}
	}
}
