﻿using System;


public class EventThrottle
{
	public event Action<EventThrottle> ThrottleStart;
	public event Action<EventThrottle> ThrottleEnd;
	public TimeSpan interval { get; private set; }
	public uint maxEvents { get; private set; }

	private uint numEventsThisInterval;
	private DateTime lastIntervalStartTime;





	public EventThrottle(double interval = float.MaxValue, uint maxEvents = int.MaxValue, Action<EventThrottle> throttleStart = null, Action<EventThrottle> throttleEnd = null)
	{
		ThrottleStart = throttleStart;
		ThrottleEnd = throttleEnd;

		Set(interval, maxEvents);
	}



	public void Set(double interval, uint maxEvents)
	{
		this.interval = interval == float.MaxValue ? TimeSpan.MaxValue : TimeSpan.FromSeconds(interval);
		this.maxEvents = maxEvents;
		Reset();
	}



	/// <summary> Blocks while the number of events triggered during the current interval would excede the limit </summary>
	public void EventAboutToOccur()
	{
		if (TimeSinceLastIntervalStart >= interval) {
			StartNewInterval();
		}

		++numEventsThisInterval;

		if (numEventsThisInterval > maxEvents)
		{
			// Wait for current interval to end
			ThrottleStart?.Invoke(this);
			while (TimeSinceLastIntervalStart < interval) { }
			ThrottleEnd?.Invoke(this);

			StartNewInterval();
			numEventsThisInterval = 1;
		}
	}



	private TimeSpan TimeSinceLastIntervalStart
	{
		get { return DateTime.Now - lastIntervalStartTime; }
	}



	public void StartNewInterval()
	{
		lastIntervalStartTime = DateTime.Now;
		numEventsThisInterval = 0;
	}



	public void Reset()
	{
		lastIntervalStartTime = DateTime.MinValue;
		numEventsThisInterval = 0;
	}
}
