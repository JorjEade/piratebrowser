﻿using System;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using TorrentBrowser;


public static class Extensions
{

	public static object CrossThreadInvoke(this Delegate delgt, params object[] args)
	{
		if (delgt == null)
			return null;

		Control control = delgt.Target as Control;

		// If it is not a control that requires invoke, just invoke the delegate directly
		if (control == null || !control.InvokeRequired)
			return delgt.Method.Invoke(delgt.Target, args);

		// Otherwise, try to invoke the delegate on the control
		if (!control.IsDisposed &&
			!control.Disposing &&
			control.Created)
		{
			return control.Invoke(delgt, args);
		}
		else
		{
			Console.WriteLine("!!! Can't invoke method on control: IsDisposed = {0}, Disposing = {1}, Created = {2}", control.IsDisposed, control.Disposing, control.Created);
			return null;
		}
	}



	public static void FillParent(this DefaultPoster targ)
	{
		targ.FillParent(DefaultPoster.defaultPosterImage);
	}



	public static void FillParent(this PictureBox targ)
	{
		targ.FillParent(targ.Image);
	}



	public static void FillParent(this Control targ, Image image)
	{
		if (targ.Parent == null || image == null) {
			return;
		}

		float ratioParent = (float)targ.Parent.ClientSize.Height / targ.Parent.ClientSize.Width;
		float ratioImage = (float)image.Height / image.Width;
			
		if (ratioImage > ratioParent)
		{
			// Image is taller than parent
			targ.Width = targ.Parent.ClientSize.Width;
			targ.Height = (int)Math.Ceiling(targ.Parent.ClientSize.Width * ratioImage);
		}
		else
		{
			// Image is wider than parent
			targ.Width = (int)Math.Ceiling(targ.Parent.ClientSize.Height / ratioImage);
			targ.Height = targ.Parent.ClientSize.Height;
		}

		// Keep centred
		targ.Left = (targ.Parent.ClientSize.Width - targ.Width) / 2;
		targ.Top = (targ.Parent.ClientSize.Height - targ.Height) / 2;
	}



	public static string CapitaliseFirstLetters(this string s)
	{
		if (string.IsNullOrEmpty(s)) {
			return string.Empty;
		}

		string[] words = s.Trim().Split(' ');

		for (int i = 0; i < words.Length; ++i) {
			words[i] = words[i][0].ToString().ToUpper() + words[i].Substring(1);
		}

		return string.Join(" ", words);
	}



	public static string ToStringReplaceNone(this AudioEncoding targ, string replaceNone)
	{
		return targ == AudioEncoding.Unknown ? replaceNone : targ.ToString();
	}



	public static string ToStringReplaceNone(this VideoEncoding targ, string replaceNone)
	{
		return targ == VideoEncoding.Unknown ? replaceNone : targ.ToString();
	}



	public static void AddAtCorrectPosition<T>(this List<T> targ, T[] items) where T : IComparable<T>
	{
		Array.ForEach(items, i => targ.AddAtCorrectPosition(i));
	}



	public static void AddAtCorrectPosition<T>(this List<T> targ, T item) where T : IComparable<T>
	{
		for (int i = 0; i < targ.Count; ++i)
		{
			if (item.CompareTo(targ[i]) == -1)
			{
				targ.Insert(i, item);
				return;
			}
		}

		targ.Add(item);
	}



	public static string TrimStart(this string targ, string s)
	{
		return targ.StartsWith(s)
			? targ.Substring(s.Length)
			: targ;
	}



	public static string TrimEnd(this string targ, string s)
	{
		return targ.EndsWith(s)
			? targ.Substring(0, targ.LastIndexOf(s))
			: targ;
	}



	public static int DistanceFromMaxScroll(this ScrollableControl targ)
	{
		return
			targ.VerticalScroll.Maximum		// Content height
			- targ.ClientSize.Height		// Viewport height
			- targ.VerticalScroll.Value;	// Content Y
	}



	public static void Scroll(this ScrollableControl targ, int amount)
	{
		targ.AutoScrollPosition = new Point(0, -(targ.AutoScrollPosition.Y - amount));
	}



	public static void PageUp(this ScrollableControl targ)
	{
		Scroll(targ, -targ.ClientSize.Height);
	}



	public static void PageDown(this ScrollableControl targ)
	{
		Scroll(targ, targ.ClientSize.Height);
	}



	public static void ScrollHome(this ScrollableControl targ)
	{
		targ.AutoScrollPosition = new Point(0, 0);
	}



	public static void ScrollEnd(this ScrollableControl targ)
	{
		targ.AutoScrollPosition = new Point(0, targ.VerticalScroll.Maximum - targ.ClientSize.Height + 1);
	}



	public static void ForEachReverse<T>(this List<T> targ, Action<T> action)
	{
		for (int i = targ.Count - 1; i >= 0; --i)
		{
			action(targ[i]);
		}
	}



	public static bool MeetsRequirements(this Torrent targ, IList<TorrentFilter> filters)
	{
		foreach (TorrentFilter filter in filters)
		{
			try
			{
				if (!filter.MeetsCriteria(targ)) {
					return false;
				}
			}
			catch (InvalidCastException) { }
		}

		return true;
	}



	public static void AddRange<T>(this List<T> targ, params T[] items)
	{
		foreach (T item in items)
		{
			targ.Add(item);
		}
	}



	public static void AddColumn(this DataGridView targ, string columnName, string headerText, int width = -1, DataGridViewAutoSizeColumnMode autoSizeMode = DataGridViewAutoSizeColumnMode.None)
	{
		int index = targ.Columns.Add(columnName, headerText);
		targ.Columns[index].Width = width;
		targ.Columns[index].AutoSizeMode = autoSizeMode;
	}



	public static void SetCell(this DataGridView targ, int rowIndex, string columnName, object value, Color? color = null, FontStyle? fontStyle = null)
	{
		DataGridViewCell cell = targ.Rows[rowIndex].Cells[columnName];

		cell.Value = value;

		// Color
		if (color.HasValue) {
			cell.Style.ForeColor = cell.Style.SelectionForeColor = color.Value;
		}

		// Font style
		if (fontStyle.HasValue) {
			cell.Style.Font = new Font(targ.Font, fontStyle.Value);
		}
	}



	public static void SetFirstSortDirectionToDesc(this DataGridView targ)
	{
		foreach (DataGridViewColumn column in targ.Columns)
		{
			column.SortMode = DataGridViewColumnSortMode.Programmatic;
		}

		targ.ColumnHeaderMouseClick += (sender, e) =>
		{
			var column = targ.Columns[e.ColumnIndex];

			if (column.SortMode != DataGridViewColumnSortMode.Programmatic)
				return;

			var sortGlyph = column.HeaderCell.SortGlyphDirection;

			switch (sortGlyph)
			{
				case SortOrder.None:
					targ.Sort(column, ListSortDirection.Descending);
					column.HeaderCell.SortGlyphDirection = SortOrder.Descending;
					break;

				case SortOrder.Descending:
					targ.Sort(column, ListSortDirection.Ascending);
					column.HeaderCell.SortGlyphDirection = SortOrder.Ascending;
					break;

				case SortOrder.Ascending:
					column.HeaderCell.SortGlyphDirection = SortOrder.None;
					break;
			}
		};
	}



	public static void MakeRightClickSelectCell(this DataGridView targ)
	{
		targ.CellMouseDown += (object sender, DataGridViewCellMouseEventArgs e) =>
		{
			switch (e.Button)
			{
				case MouseButtons.Right:
					if (e.RowIndex < 0) return;
					targ.CurrentCell = targ.Rows[e.RowIndex].Cells[e.ColumnIndex]; 
					targ.Rows[e.RowIndex].Selected = true; 
					targ.Focus();
					break;
			}
		};
	}



	public static void RestrictToNumbers(this TextBox targ)
	{
		targ.KeyPress += (o, e) =>
		{
			e.Handled =
				!Regex.IsMatch(e.KeyChar.ToString(), @"[0-9]", RegexOptions.IgnoreCase) &&
				!char.IsControl(e.KeyChar);
		};
	}



	public static string[] ToArray(this string targ)
	{
		string[] ret = targ.Split(',');

		for (int i = ret.Length - 1; i >= 0; --i)
			ret[i] = ret[i].Trim();

		return ret;
	}



	public static void SetSelectedValue<T>(this ComboBox targ, T value)
	{
		EqualityComparer<T> comparer = EqualityComparer<T>.Default;

		for (int i = targ.Items.Count - 1; i >= 0; --i)
		{
			ComboBoxItem<T> item = (ComboBoxItem<T>)targ.Items[i];

			if (item != null && comparer.Equals(item.Value, value)) {
				targ.SelectedIndex = i;
				return;
			}
		}

		throw new Exception("Item \"" + value + "\" not found");
	}




	public static bool Contains_IgnoreCase(this IList<string> targ, string item)
	{
		for (int i = targ.Count - 1; i >= 0; --i)
			if (string.Equals(targ[i], item, StringComparison.OrdinalIgnoreCase))
				return true;

		return false;
	}



	public static void DisposeAndClear<T>(this IList<T> targ) where T : IDisposable
	{
		for (int i = 0; i < targ.Count; ++i)
			targ[i].Dispose();

		targ.Clear();
	}

}