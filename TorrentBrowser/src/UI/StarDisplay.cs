﻿using System;
using System.Drawing;


namespace TorrentBrowser
{
	class StarDisplay : TransparentDrawingArea
	{

		public enum AnchorH
		{
			Left,
			Centre,
			Right
		}

		public enum AnchorV
		{
			Top,
			Centre,
			Bottom
		}

		private static readonly Font fontNormal = new Font("Calibri", 15);
		private static readonly Image starEmpty = Utils.LoadImageAsset("Resources/Images/StarEmpty.png");
		private static readonly Image starHalf = Utils.LoadImageAsset("Resources/Images/StarHalf.png");
		private static readonly Image starFull = Utils.LoadImageAsset("Resources/Images/StarFull.png");
		private static readonly SolidBrush whiteBrush = new SolidBrush(Color.White);
		private static readonly Size starImageSize = starEmpty.Size;
				
		public float normScore = 0;
		public int numStars = 5;
		public int starSpacing = 30;
		public bool showPercent = true;
		private float starScale;



		public StarDisplay()
		{
			Size = new Size(300, starImageSize.Height);
		}



		protected override void OnSizeChanged(EventArgs e)
		{
			starScale = Size.Height / (float)starImageSize.Height;
		}



		protected override void OnDraw()
		{
			// Stars
			for (int i = 0; i < numStars; ++i)
			{
				float normStarFill = (normScore * numStars) - i;

				graphics.DrawImage(
					image:	normStarFill < 0.25f ? starEmpty : normStarFill < 0.75 ? starHalf : starFull,
					x:		starSpacing * i * starScale,
					y:		0,
					width:	starImageSize.Width * starScale,
					height:	starImageSize.Height * starScale
				);
			}

			
			// Percent
			if (showPercent)
			{
				graphics.DrawString(
					Math.Round(normScore * 100) + "%",
					fontNormal,
					whiteBrush,
					new Rectangle(152, 2, 100, 30),
					null
				);
			}
		}



		public Size StarsArea
		{
			get
			{
				return new Size(
					(int)(starScale * (starImageSize.Width + starSpacing * (numStars - 1))),
					(int)(starScale * (starImageSize.Height))
				);
			}
		}

	}
}
