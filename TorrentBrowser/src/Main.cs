﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;


namespace TorrentBrowser
{

	public class Main : Form
	{

		private const string DEFAULT_PIRATE_BAY_DOMAIN = "https://pirateproxy.live";
		private const string BLOCKED_UPLOADERS_PATH = @"D:\Documents\TorrentBrowser\TorrentBrowser\Resources\ExcludedUploaders.txt";

		public static Main Instance { get; private set; }
		private Timer checkScrollTimer;
		private Timer addPostersTimer;
		private readonly List<PirateSearch> pirateSearches = new List<PirateSearch>();
		private PosterGrid posterGrid;
		private Animation.Token fadeInAnim;
		private readonly List<VideoInfoPopup> infoPopups = new List<VideoInfoPopup>();
		public static readonly List<string> excludedUploaders = new List<string>(File.ReadAllLines(BLOCKED_UPLOADERS_PATH));
		private readonly SearchForm searchForm = new SearchForm();
		private readonly Curtain backgroundInvisibleButton;
		private readonly BooleanStateRequest backgroundDimmed = new BooleanStateRequest();
		private readonly BooleanStateRequest loadingCursorShown = new BooleanStateRequest();
		private readonly ContentDatabase contentDatabase = new ContentDatabase();
		private int numPostersRequired;
		
		
		
		public Main()
		{
			Instance = this;

			backgroundDimmed.onStateChanged += OnBackgroundDimChanged;
			loadingCursorShown.onStateChanged += OnLoadingCursorShownChanged;

			// Initialise form
			SuspendLayout();
			//ComponentResourceManager resources = new ComponentResourceManager(typeof(Main));
			//Icon = (Icon)resources.GetObject("$this.Icon");
			ControlBox = false;
			Name = "TorrentBrowser";
			FormBorderStyle = FormBorderStyle.None;
			WindowState = FormWindowState.Maximized;
			Opacity = 0;
			KeyPreview = true;
			BackColor = Color.Black;
			ResumeLayout(false);
			

			// Create background dimmer
			backgroundInvisibleButton = new Curtain();
			Controls.Add(backgroundInvisibleButton);
			backgroundInvisibleButton.Visible = false;
			backgroundInvisibleButton.Cursor = Cursors.Hand;
			backgroundInvisibleButton.Click += (s, e) => {
				infoPopups.ForEachReverse(p => p.Close());
				searchForm.Hide();
			};

			
			// Create poster grid
			posterGrid = new PosterGrid();
			posterGrid.PosterClicked += OnPosterClicked;
			Controls.Add(posterGrid);


			// Create search form
			searchForm.Search += OnSearchFormSearch;
			searchForm.Hidden += OnSearchFormHidden;


			// Start checking if poster container scrolled to end
			checkScrollTimer = new Timer();
			checkScrollTimer.Interval = 500;
			checkScrollTimer.Tick += CheckIfScrolledToEnd;
			checkScrollTimer.Start();


			// Start checking if poster container scrolled to end
			addPostersTimer = new Timer();
			addPostersTimer.Interval = 500;
			addPostersTimer.Tick += CheckAddMorePosters;
			addPostersTimer.Start();
		}

		

		protected override void OnLoad(EventArgs e)
		{
			fadeInAnim = Animation.Tween(
				onUpdate:	val => Opacity = val,
				from:		0,
				to:			1,
				duration:	1
			);

			posterGrid.OnParentClientSizeChanged();

			// Search
			DefaultSearch();
			//LoadSampleRawTitles();
			//ShowSearchForm();
			//NewSearch(PirateSearch.GetPath_Search("asdasd", (int)PirateCategory.Category.Video));
		}



		private void DefaultSearch()
		{
			NewSearch(
				new string[] {
					PirateSearch.GetPath_BrowseCategory((int)PirateCategory.SubcategoryVideo.Movies),
					PirateSearch.GetPath_BrowseCategory((int)PirateCategory.SubcategoryVideo.HDMovies)
				},
				new TorrentFilter_Video() { uploaderExclude = excludedUploaders }
			);
		}



		private void ClearSearch()
		{
			numPostersRequired = 0;
			contentDatabase.CancelLoad();
			contentDatabase.ClearContents();
			posterGrid.Clear();
			CancelLoading();
		}



		private void CancelLoading()
		{
			pirateSearches.DisposeAndClear();
			contentDatabase.CancelLoad();
		}


		private void NewSearch(string paths, params TorrentFilter[] filters)
		{
			NewSearch(new string[] { paths }, filters);
		}



		private void NewSearch(string[] paths, params TorrentFilter[] filters)
		{
			ClearSearch();

			foreach (var path in paths)
			{
				var search = new PirateSearch(DEFAULT_PIRATE_BAY_DOMAIN, path);
				pirateSearches.Add(search);
				search.TorrentCreated += t => { if (t.MeetsRequirements(filters)) contentDatabase.AddFromTorrent(t); };
				loadingCursorShown.AddContraryStateRequest(search);
				search.Load(s => loadingCursorShown.RemoveContraryStateRequest(s));
			}
		}



		private void OnPosterClicked(Poster poster)
		{
			VideoInfoPopup popup = infoPopups.Find(i => i.Video == poster.Video);

			if (popup != null) {
				popup.Select();
				return;
			}

			popup = new VideoInfoPopup(poster.Video);
			popup.ClientSize = new Size((int)(posterGrid.PosterSize.Width * 3.5), posterGrid.PosterSize.Height);
			popup.Show(this);
			popup.FormClosed += OnPopupClosed;
			infoPopups.Add(popup);
			backgroundDimmed.AddContraryStateRequest(popup);
		}



		private void OnPopupClosed(object o, FormClosedEventArgs e)
		{
			infoPopups.Remove((VideoInfoPopup)o);
			backgroundDimmed.RemoveContraryStateRequest(o);
		}



		private void CheckIfScrolledToEnd(object sender, EventArgs e)
		{
			double loadNextPageWhenDistanceFromMaxScrollIsLessThan = posterGrid.PosterSize.Height * 2;
			int numPagesRequired = (int)Math.Ceiling((posterGrid.VerticalScroll.Value + posterGrid.ClientSize.Height + loadNextPageWhenDistanceFromMaxScrollIsLessThan) / posterGrid.PageSize.Height);
			numPostersRequired = Math.Max(numPostersRequired, numPagesRequired * posterGrid.PostersPerPage);
		}



		private void CheckAddMorePosters(object sender, EventArgs e)
		{
			for (int i = posterGrid.Posters.Count; i < Math.Min(contentDatabase.Contents.Count, numPostersRequired); ++i)
			{
				Video video = contentDatabase.Contents[i] as Video;

				if (video != null) {
					Poster poster = posterGrid.AddPoster(video);
					poster.Dimmed = backgroundDimmed;
				}
			}
		}



		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);
			
			switch (e.KeyCode)
			{
				case Keys.Escape:
					Application.Exit();
					break;

				case Keys.Down:
					Extensions.Scroll(posterGrid, 20);
					break;

				case Keys.Up:
					Extensions.Scroll(posterGrid, -20);
					break;
					
				case Keys.PageDown:
					posterGrid.PageDown();
					break;

				case Keys.PageUp:
					posterGrid.PageUp();
					break;
					
				case Keys.Home:
					posterGrid.ScrollHome();
					break;
					
				case Keys.End:
					posterGrid.ScrollEnd();
					break;

				case Keys.Delete:
					CancelLoading();
					break;

				case Keys.F:
					ShowSearchForm();
					break;
			}
		}



		private void OnBackgroundDimChanged(bool newValue)
		{
			posterGrid.SetDimmed(newValue);
			backgroundInvisibleButton.Visible = newValue;
			Refresh();
		}



		private void OnLoadingCursorShownChanged(bool newValue)
		{
			Cursor = newValue
				? Cursors.AppStarting
				: Cursors.Default;
		}



		private void ShowSearchForm()
		{
			searchForm.Show(this);
			backgroundDimmed.AddContraryStateRequest(searchForm);
		}



		private void OnSearchFormHidden(SearchForm form)
		{
			backgroundDimmed.RemoveContraryStateRequest(form);
		}



		private void OnSearchFormSearch(SearchForm form)
		{
			NewSearch(
				form.GetSearchUrls(),
				form.GetSearchFilter()
			);
		}



		public bool IsUploaderBlocked(string name)
		{
			return excludedUploaders.Contains(name);
		}



		public void SetUploaderBlocked(string name, bool blocked)
		{
			if (string.IsNullOrEmpty(name))
				return;

			if (blocked == IsUploaderBlocked(name))
				return;

			if (blocked)
				excludedUploaders.Add(name); else
				excludedUploaders.Remove(name);

			File.WriteAllLines(BLOCKED_UPLOADERS_PATH, excludedUploaders);
		}



		void LoadSampleRawTitles()
		{
			string[] rawTitles = File.ReadAllLines(@"D:\Documents\TorrentBrowser\TorrentBrowser\Resources\RawTitles.txt");

			for (int i = 0; i < Math.Min(2000, rawTitles.Length); ++i)
			{
				string rawTitle = rawTitles[i];
				Torrent_Video t = new Torrent_Video(VideoType.Movie, 0, 0, "", rawTitle, "", 0, false, false, new DateTime(), 0, "", 0, 0);
				Console.WriteLine(t.rawTitle.PadRight(64) + ": " + t.season + ", " + t.seasonMax + ", " + t.episode + ", " + t.episodeMax);
			}
		}



		protected override void Dispose(bool disposing)
		{
			ClearSearch();
			searchForm?.Dispose();
			fadeInAnim?.Cancel();
			checkScrollTimer?.Stop();
			checkScrollTimer?.Dispose();
			contentDatabase?.Dispose();
			posterGrid.Dispose();
		}
	}
}
