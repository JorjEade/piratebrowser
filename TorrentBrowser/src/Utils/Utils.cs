﻿using System;
using System.Threading;
using System.Net;
using System.Text;
using System.Reflection;
using System.Drawing;


public static class Utils
{

	public static WebClient LoadDataAsync(string uri, Action<string> onLoaded, Action<Exception> onError = null)
	{
		using (WebClient client = new WebClient())
		{
			client.DownloadStringCompleted += (object sender, DownloadStringCompletedEventArgs e) =>
			{
				if (e.Error != null)
					onError(e.Error);
				else
					onLoaded(e.Result);
			};
			
			client.DownloadStringAsync(new Uri(uri));

			return client;
		}
	}


	
	public static string LoadData(string uri, Encoding encoding = null)
	{
		using (WebClient client = new WebClient() { Encoding = encoding ?? Encoding.Default} )
			return client.DownloadString(uri);
	}



	public static DateTime FromUnixEpoch(int seconds)
	{
		return new DateTime(1970, 1, 1).AddSeconds(seconds);
	}



	public static Image LoadImageAsset(string path, string @namespace = "TorrentBrowser")
	{
		return Image.FromStream(Assembly.GetExecutingAssembly().GetManifestResourceStream(@namespace + "." + path.Replace('/', '.')));
	}



	public static Thread ThreadedJob(Action job, Action onComplete = null, Action<Exception> onException = null)
	{
		ThreadPool.QueueUserWorkItem
		(
			state =>
			{
				try
				{
					job();
				}
				catch (Exception e)
				{
					if (onException != null)
					{
						onException(e);
						return;
					}
					else
					{
						throw;
					}
				}

				onComplete?.Invoke();
			}
		);

		return null;
	}



	public static int KBtoBytes(float kb)
	{
		return (int)(1024 * kb);
	}



	public static int MBtoBytes(float mb)
	{
		return (int)(1024 * 1024 * mb);
	}



	public static long GBtoBytes(float gb)
	{
		return (long)(1024L * 1024 * 1024 * gb);
	}



	public static long TBtoBytes(float tb)
	{
		return (long)(1024L * 1024 * 1024 * 1024 * tb);
	}

}

