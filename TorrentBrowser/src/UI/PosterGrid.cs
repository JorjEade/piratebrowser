﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;


namespace TorrentBrowser
{
	class PosterGrid : Panel_FixedAutoScroll
	{

		public int RowsPerPage { get; private set; }
		public int ColsPerPage { get; private set; }
		public int PostersPerPage { get; private set; }
		public List<Poster> Posters { get; private set; } = new List<Poster>();
		public event Action<Poster> PosterClicked;
		public Size PosterSize { get { return posterSize; } }
		public Size PageSize { get { return pageSize; } }

		private Size posterSize;
		private Size pageSize;

		

		public PosterGrid(int rowsPerPage = 3, int colsPerPage = 6)
		{
			RowsPerPage = rowsPerPage;
			ColsPerPage = colsPerPage;
			PostersPerPage = rowsPerPage * colsPerPage;

			Dock = DockStyle.Fill;
			AutoScroll = true;
		}


		public void OnParentClientSizeChanged()
		{
			// Set poster size
			posterSize.Width = (ClientSize.Width - SystemInformation.VerticalScrollBarWidth) / ColsPerPage;
			posterSize.Height = (int)(PosterSize.Width * Poster.WIDTH_TO_HEIGHT);

			// Set page size
			pageSize.Width = PosterSize.Width * ColsPerPage;
			pageSize.Height = PosterSize.Height * RowsPerPage;
		}



		public Poster AddPoster(Video video)
		{
			Poster poster = new Poster(PosterSize.Width, PosterSize.Height, video);
			int posterIndex = Posters.Count;
			int row = posterIndex / ColsPerPage;
			int col = posterIndex % ColsPerPage;
			Controls.Add(poster);
			poster.Location = new Point(col * PosterSize.Width, row * PosterSize.Height - VerticalScroll.Value);
			poster.Clicked += OnPosterClicked;
			Posters.Add(poster);
			return poster;
		}


		private void DestroyPoster(Poster poster)
		{
			Posters.Remove(poster);
			Controls.Remove(poster);
			poster.Clicked -= OnPosterClicked;
			poster.Dispose();
		}



		private void OnPosterClicked(Poster poster)
		{
			PosterClicked?.Invoke(poster);
		}



		public void SetDimmed(bool dimmed)
		{
			Posters.ForEach(p => p.Dimmed = dimmed);
		}



		public void Clear()
		{
			for (int i = Posters.Count - 1; i >= 0; --i)
				DestroyPoster(Posters[i]);
		}



		public new void Dispose()
		{
			Clear();

			base.Dispose();
		}

	}
}
