﻿using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;


public abstract class TransparentDrawingArea : Panel
{
	protected Graphics graphics;

	protected abstract void OnDraw();

	protected override CreateParams CreateParams
	{
		get
		{
			CreateParams cp = base.CreateParams;
			cp.ExStyle |= 0x00000020; //WS_EX_TRANSPARENT

			return cp;
		}
	}

	protected override void OnPaintBackground(PaintEventArgs pevent)
	{
		// Don't paint background
	}

	protected override void OnPaint(PaintEventArgs e)
	{
		graphics = e.Graphics;

		// Set the best settings possible (quality-wise)
		graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
		graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
		graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
		graphics.SmoothingMode = SmoothingMode.HighQuality;

		OnDraw();
	}
}
