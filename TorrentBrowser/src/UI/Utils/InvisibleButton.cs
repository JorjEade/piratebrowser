﻿using System.Drawing;
using System.Windows.Forms;


public class InvisibleButton : Control
{

	public InvisibleButton()
	{
		SetStyle(ControlStyles.SupportsTransparentBackColor, true);
		SetStyle(ControlStyles.Opaque, true);
		BackColor = Color.Transparent;
		Cursor = Cursors.Hand;
	}

	protected override CreateParams CreateParams
	{
		get
		{
			CreateParams cp = base.CreateParams;
			cp.ExStyle = cp.ExStyle | 0x20;
			return cp;
		}
	}
}