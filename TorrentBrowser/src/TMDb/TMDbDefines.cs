﻿
namespace TorrentBrowser
{
	public enum ContentType
	{
		movie,
		tv
	}

	public enum PosterSize
	{
		w92,
		w154,
		w185,
		w342,
		w500,
		w780,
		original
	}

	public enum BackdropSize
	{
		w300,
		w780,
		w1280,
		original
	}


	public static class TMDbDefines
	{
		public const string API_KEY = "15d2ea6d0dc1d476efbca3eba2b9bbfb";
		public const string IMAGE_URL_FORMAT = "https://image.tmdb.org/t/p/{0}";
	}


}
