﻿using System;
using System.Threading;
using System.Collections.Generic;


namespace TorrentBrowser
{

	public abstract class Content : IDisposable
	{

		public readonly List<Torrent> Torrents = new List<Torrent>();
		public readonly string id;
		public readonly string title;
		




		public Content(string id, string title = null, params Torrent[] torrents)
		{
			this.id = id;
			this.title = title;
			
			AddTorrent(torrents);
		}



		public void AddTorrent(params Torrent[] torrents)
		{
			Torrents.AddAtCorrectPosition(torrents);
		}



		public bool TryAddTorrent(Torrent torrent)
		{
			if (TorrentBelongs(torrent))
			{
				AddTorrent(torrent);
				return true;
			}

			return false;
		}



		/// <summary> Returns true if the torrent's IdForContent matches that of one of our existing torrents </summary>
		public bool TorrentBelongs(Torrent torrent)
		{
			return Torrents.Exists(t => t.IdForContent == torrent.IdForContent);
		}



		public virtual bool IsSameAs(Content other)
		{
			return
				GetType() == other.GetType() &&
				StringUtils.Compare(id, other.id);
		}
		


		public virtual void Dispose() { }





		// --------------------------- STATIC HELPERS ---------------------------
		
		public static Content CreateFromTorrent(Torrent torrent, CancellationTokenSource cancellationToken = null)
		{
			Torrent_Video torrentAsVideo = torrent as Torrent_Video;

			if (torrentAsVideo != null)
				return CreateFromTorrent_Video(torrentAsVideo, cancellationToken);

			//Console.WriteLine("Unsupported Torrent type: " + torrent.GetType() + "(" + (PirateBay.PirateCategory.Category)torrent.category + ")");

			return null;
		}



		public static Video CreateFromTorrent_Video(Torrent_Video torrent, CancellationTokenSource cancellationToken = null)
		{
			// Do a database search for the video's name and date
			TMDbSearchResponse.Result result = null;
			try
			{
				result = TMDbSearch.DoSearch(
					torrent.title,
					torrent.releaseDate == Video.NoReleaseDate ? 0 : torrent.releaseDate.Year,
					torrent.videoType == VideoType.Movie ? ContentType.movie : ContentType.tv,
					cancellationToken
				);
			}
			catch (Exception e)
			{
				Console.WriteLine("Error with TMDb search for torrent \"" + torrent.rawTitle + "\":\n   \"" + e.Message + "\"");
			}

			// Check if cancelled
			if (cancellationToken != null && cancellationToken.IsCancellationRequested)
				return null;

			if (result == null)
			{
				// Search failed - create a new video from the data we have
				return new Video(
					id:				torrent.title,
					title:			torrent.title.CapitaliseFirstLetters(),
					releaseDate:	torrent.releaseDate,
					torrents:		new Torrent_Video[] { torrent }
				);
			}
			else
			{
				// Search succeeded - create a new video from its first result
				return new Video(
					id:					result.id.ToString(),
					title:				result.title ?? result.name,
					releaseDate:		result.Date,
					description:		result.overview,
					normScore:			result.vote_average / 10,
					genres:				result.GetGenreArray(),
					posterUrlFormat:	!string.IsNullOrEmpty(result.poster_path) ? TMDbDefines.IMAGE_URL_FORMAT + result.poster_path : "",
					backdropUrlFormat:	!string.IsNullOrEmpty(result.backdrop_path) ? TMDbDefines.IMAGE_URL_FORMAT + result.backdrop_path : "",
					torrents:			new Torrent_Video[] { torrent }
				);
			}
		}

	}

}
