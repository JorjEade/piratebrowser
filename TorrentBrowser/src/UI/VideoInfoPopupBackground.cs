﻿using System;
using System.Drawing;


namespace TorrentBrowser
{
	class VideoInfoPopupBackground : TransparentDrawingArea
	{

		private static readonly Image dark = Utils.LoadImageAsset("Resources/Images/Dark.png");
		private static readonly StringFormat stringFormatTitle = new StringFormat(StringFormatFlags.NoWrap) { Trimming = StringTrimming.EllipsisCharacter };
		private static readonly StringFormat stringFormatDesc = new StringFormat(StringFormatFlags.NoClip) { Trimming = StringTrimming.EllipsisWord };
		private static readonly StringFormat stringFormatInfoColL = new StringFormat() { Alignment = StringAlignment.Far };
		private static readonly StringFormat stringFormatInfoColR = new StringFormat() { Alignment = StringAlignment.Near };
		private static readonly SolidBrush whiteBrush = new SolidBrush(Color.White);
		private static readonly SolidBrush greyBrush = new SolidBrush(Color.FromArgb(102, 255, 255, 255));
		private static readonly Font fontTitle = new Font("Calibri", 35);
		private static readonly Font fontNormal = new Font("Calibri", 15);

		private Video video;
		private StarDisplay starDisplay;



		public VideoInfoPopupBackground(Video video = null)
		{
			starDisplay = new StarDisplay();
			starDisplay.Location = new Point(25, 70);
			Controls.Add(starDisplay);

			if (video != null) {
				SetVideo(video);
			}
		}



		public void SetVideo(Video video = null)
		{
			this.video = video;

			// Set stars display
			if (starDisplay.Visible = video.normScore > 0) {
				starDisplay.normScore = video.normScore;
			}
		}



		protected override void OnDraw()
		{
			if (video == null) {
				return;
			}

			// Dark
			graphics.DrawImage(dark, ClientRectangle);
			

			// Title
			graphics.DrawString(
				video.titleAndYear,
				fontTitle,
				whiteBrush,
				new Rectangle(VideoInfoPopup.PADDING - 5, VideoInfoPopup.PADDING - 10, ClientSize.Width - VideoInfoPopup.PADDING, 100),
				stringFormatTitle
			);


			// Genres
			if (video.genres != null && video.genres.Length > 0)
			{
				graphics.DrawString(
					string.Join(", ", video.genres),
					fontNormal,
					greyBrush,
					new Rectangle(
						VideoInfoPopup.PADDING,
						VideoInfoPopup.PADDING + 100,
						ClientSize.Width - VideoInfoPopup.PADDING * 2,
						30
					),
					null
				);
			}
			

			// Description
			if (!string.IsNullOrEmpty(video.description))
			{
				graphics.DrawString(
					video.description,
					fontNormal,
					whiteBrush,
					new Rectangle(
						VideoInfoPopup.PADDING,
						VideoInfoPopup.PADDING + 128,
						ClientSize.Width - VideoInfoPopup.PADDING * 2,
						ClientSize.Height - 146 - 30 - (VideoInfoPopup.PADDING + 128)
					),
					stringFormatDesc
				);
			}
		}

	}

}
