﻿using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;


namespace TorrentBrowser
{
	public class DefaultPoster : Panel
	{

		private const float SHADE_HEIGHT_TOP_PERCENT = 0.45f;
		private const float SHADE_HEIGHT_BOTTOM_PERCENT = 0.45f;

		public static readonly Image defaultPosterImage = Utils.LoadImageAsset("Resources/Images/DefaultMoviePoster.png");
		private static readonly StringFormat stringFormatTitle = new StringFormat() { Alignment = StringAlignment.Center };
		private static readonly SolidBrush grayBrush = new SolidBrush(Color.Gray);
		private static readonly Font fontTitle = new Font("Calibri", 20, FontStyle.Bold);

		private Video video;




		public DefaultPoster(Video video = null)
		{
			if (video != null)
			{
				SetVideo(video);
			}
		}



		public void SetVideo(Video video)
		{
			this.video = video;
		}


		protected override void OnPaint(PaintEventArgs e)
		{
			Graphics graphics = e.Graphics;

			// Set the best settings possible (quality-wise)
			graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
			graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
			graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
			graphics.SmoothingMode = SmoothingMode.HighQuality;


			// Poster image
			graphics.DrawImage(defaultPosterImage, ClientRectangle);
			
			const int PADDING = 10;

			// Title
			if (video != null)
			{
				graphics.DrawString(
					video.titleAndYear,
					fontTitle,
					grayBrush,
					new RectangleF(PADDING, PADDING, Width - 2 * PADDING, Height - 2 * PADDING),
					stringFormatTitle
				);
			}
		}
	}
}
