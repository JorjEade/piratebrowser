﻿using System;
using System.Linq;


namespace TorrentBrowser
{
	public static class PirateCategory
	{

		public static readonly int[] MOVIE_CATEGORIES = new int[] {
			(int)SubcategoryVideo.Movies,
			(int)SubcategoryVideo.MoviesDVDR,
			(int)SubcategoryVideo.MusicVideos,
			(int)SubcategoryVideo.HandHeld,
			(int)SubcategoryVideo.HDMovies,
			(int)SubcategoryVideo.ThreeDee,
			(int)SubcategoryVideo.Other,
			(int)SubcategoryPorn.Movies,
			(int)SubcategoryPorn.HDMovies,
			(int)SubcategoryPorn.MoviesDVDR,
		};

		public static readonly int[] TV_CATEGORIES = new int[] {
			(int)SubcategoryVideo.TVShows,
			(int)SubcategoryVideo.HDTVShows,
		};



		public enum Category
		{
			Unknown = 0,
			Audio = 100,
			Video = 200,
			Applications = 300,
			Games = 400,
			Porn = 500,
			Other = 600
		}

		public enum SubcategoryAudio
		{
			Unknown = 0,
			Music = 101,
			AudioBooks = 102,
			SoundClips = 103,
			FLAC = 104,
			Other = 199
		}

		public enum SubcategoryVideo
		{
			Unknown = 0,
			Movies = 201,
			MoviesDVDR = 202,
			MusicVideos = 203,
			MovieClips = 204,
			TVShows = 205,
			HandHeld = 206,
			HDMovies = 207,
			HDTVShows = 208,
			ThreeDee = 209,
			Other = 299
		}

		public enum SubcategoryApplications
		{
			Unknown = 0,
			Windows = 301,
			Mac = 302,
			UNIX = 303,
			Handheld = 304,
			IOS = 305,
			Android = 306,
			Other = 399,
		}

		public enum SubcategoryGames
		{
			Unknown = 0,
			PC = 401,
			Mac = 402,
			PSx = 403,
			XBOX360 = 404,
			Wii = 405,
			Handheld = 406,
			IOS = 407,
			Android = 408,
			Other = 499
		}

		public enum SubcategoryPorn
		{
			Unknown = 0,
			Movies = 501,
			MoviesDVDR = 502,
			Picture = 503,
			Games = 504,
			HDMovies = 505,
			MovieClips = 506,
			Other = 599
		}

		public enum SubcategoryOther
		{
			Unknown = 0,
			EBooks = 601,
			Comics = 602,
			Picture = 603,
			Covers = 604,
			Physibles = 605,
			Other = 699
		}



		public static Category ParseCategory(string input)
		{
			try
			{
				return (Category)Enum.Parse(typeof(Category), input, ignoreCase: true);
			}
			catch (Exception)
			{
				return Category.Unknown;
			}
		}



		public static SubcategoryAudio ParseSubcategoryAudio(string input)
		{
			switch (input)
			{
				case "Music":				return SubcategoryAudio.Music;
				case "Audio books":			return SubcategoryAudio.AudioBooks;
				case "Sound clips":			return SubcategoryAudio.SoundClips;
				case "FLAC":				return SubcategoryAudio.FLAC;
				case "Other":				return SubcategoryAudio.Other;
				default:					return SubcategoryAudio.Unknown;
			}
		}



		public static SubcategoryVideo ParseSubcategoryVideo(string input)
		{
			switch (input)
			{
				case "Movies":				return SubcategoryVideo.Movies;
				case "Movies DVDR":			return SubcategoryVideo.MoviesDVDR;
				case "Music videos":		return SubcategoryVideo.MusicVideos;
				case "Movie clips":			return SubcategoryVideo.MovieClips;
				case "TV shows":			return SubcategoryVideo.TVShows;
				case "Handheld":			return SubcategoryVideo.HandHeld;
				case "HD - Movies":			return SubcategoryVideo.HDMovies;
				case "HD - TV shows":		return SubcategoryVideo.HDTVShows;
				case "3D":					return SubcategoryVideo.ThreeDee;
				case "Other":				return SubcategoryVideo.Other;
				default:					return SubcategoryVideo.Unknown;
			}
		}



		public static SubcategoryApplications ParseSubcategoryApplications(string input)
		{
			switch (input)
			{
				case "Windows":				return SubcategoryApplications.Windows;
				case "Mac":					return SubcategoryApplications.Mac;
				case "UNIX":				return SubcategoryApplications.UNIX;
				case "Handheld":			return SubcategoryApplications.Handheld;
				case "IOS (iPad/iPhone)":	return SubcategoryApplications.IOS;
				case "Android":				return SubcategoryApplications.Android;
				case "Other OS":			return SubcategoryApplications.Other;
				default:					return SubcategoryApplications.Unknown;
			}
		}



		public static SubcategoryGames ParseSubcategoryGames(string input)
		{
			switch (input)
			{
				case "PC":					return SubcategoryGames.PC;
				case "Mac":					return SubcategoryGames.Mac;
				case "PSx":					return SubcategoryGames.PSx;
				case "XBOX360":				return SubcategoryGames.XBOX360;
				case "Wii":					return SubcategoryGames.Wii;
				case "Handheld":			return SubcategoryGames.Handheld;
				case "IOS (iPad/iPhone)":	return SubcategoryGames.IOS;
				case "Android":				return SubcategoryGames.Android;
				case "Other":				return SubcategoryGames.Other;
				default:					return SubcategoryGames.Unknown;
			}
		}



		public static SubcategoryPorn ParseSubcategoryPorn(string input)
		{
			switch (input)
			{
				case "Movies":				return SubcategoryPorn.Movies;
				case "Movies DVDR":			return SubcategoryPorn.MoviesDVDR;
				case "Pictures":			return SubcategoryPorn.Picture;
				case "Games":				return SubcategoryPorn.Games;
				case "HD - Movies":			return SubcategoryPorn.HDMovies;
				case "Movie clips":			return SubcategoryPorn.MovieClips;
				case "Other":				return SubcategoryPorn.Other;
				default:					return SubcategoryPorn.Unknown;
			}
		}



		public static SubcategoryOther ParseSubcategoryOther(string input)
		{
			switch (input)
			{
				case "E-books":				return SubcategoryOther.EBooks;
				case "Comics":				return SubcategoryOther.Comics;
				case "Pictures":			return SubcategoryOther.Picture;
				case "Covers":				return SubcategoryOther.Covers;
				case "Physibles":			return SubcategoryOther.Physibles;
				default:					return SubcategoryOther.Unknown;
			}
		}



		public static int ParseSubcategory(Category category, string input)
		{
			switch (category)
			{
				case Category.Audio:		return (int)ParseSubcategoryAudio(input);
				case Category.Video:		return (int)ParseSubcategoryVideo(input);
				case Category.Applications:	return (int)ParseSubcategoryApplications(input);
				case Category.Games:		return (int)ParseSubcategoryGames(input);
				case Category.Porn:			return (int)ParseSubcategoryPorn(input);
				case Category.Other:		return (int)ParseSubcategoryOther(input);
				default: return -1;
			}
		}
	}
}
